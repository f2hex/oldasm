; =======================================================================
;   TRACKER -- Interrupts Services Interceptor
; -----------------------------------------------------------------------
;
;                       (C) Copyright  ARTIS 1991
;
; -----------------------------------------------------------------------
;   module:      TRACKER
;   description: Interrupts Services Interceptor
;   created by:  Franco Fiorese
;   date:        10-Jul-91
; -----------------------------------------------------------------------
;   Note: This program requires a 286 or greater processor
; -----------------------------------------------------------------------
;   $Log$
; =======================================================================
;
;
.286
NAME           tracker
               LOCALS                  ; enable use of local labels
include        global.inc
;
;
; =======================================================================
;   print_string -- writes an ASCIIZ string to DOS console
; =======================================================================
;
.print_string  macro   @string
               push    ds
               mov     dx,seg TRANSIENT_DATA
               mov     ds,dx
               lea     dx,TRANSIENT_DATA:[@string]
               call    WrAsciiz        ;; writes an ASCII string
               pop     ds
               endm
;
; =======================================================================
;   .dispvalhdr -- macro used to display a 3 bytes string on video ram
; =======================================================================
;
.dispvalhdr    macro @vattr
               ifnb <@vattr>
                 mov ah,@vattr         ;; video attribute
               endif
                 rept 2
                   lodsb
                   stosw
                 endm
                 mov   al,'='
                 stosw
               endm
;
; =======================================================================
;   ITCBE -- macro used to declare an ITCB record entry
; -----------------------------------------------------------------------
;   note: this macro must respect the structure items ordering: ITCB
;         defined below
; =======================================================================
;
ITCBE          macro   @name, @num, @mcat, @scat, @flags
IT_&@name:     call    Trapper                   ;; near call
               dw      0                         ;; wOOfs
               dw      0                         ;; wOSeg
               db      @num                      ;; bInt
               dw      RESIDENT_GROUP:@mcat      ;; wMCOfs
               dw      RESIDENT_GROUP:@scat      ;; wSCOfs
               db      @flags                    ;; bFlags
               dw      0                         ;; wCalls
               dw      0                         ;; wUSOfs
               dw      0                         ;; wUSSeg
               dw      0                         ;; wBSOfs
               endm
;
; =======================================================================
;   .data_ofs -- offset of data into RESIDENT_GROUP
; =======================================================================
;
.data_ofs      macro @data
               dw    offset RESIDENT_GROUP:@data
               endm
;
; -----------------------------------------------------------------------
;   generic useful definitions
; -----------------------------------------------------------------------
;
TSR_STACKSIZE  equ     2048            ; resident stack area size (words)
TRANSTACKSIZE  equ     128             ; transient stack area size (words)
MULTIPLEXID    equ     0FEh            ; unique INT 2Fh ID value
MPEXINTFUNC    equ     0FE05h          ; MPEX TRACKER internal functions
MAXFUNC        equ     8               ; API max implemented functions
TRUE           equ     -1
FALSE          equ     0
FLAG_ON_ATR    equ     0fh             ; CPU flags on color
FLAG_OF_ATR    equ     07h             ; CPU flags off color
REGVALATTR     equ     07              ; registers value video attribute
FNBITMAPSIZE   equ     256 / 8         ; functions codes bitmap array size
OUTBUFLEN      equ     80              ; output buffer size (in words)
POPUPTICKS     equ     18              ; ticks allowed until pop-up
;
; -----------------------------------------------------------------------
;   TRACKER system flags definitions
; -----------------------------------------------------------------------
;

;
; -----------------------------------------------------------------------
;   API calls definitions
; -----------------------------------------------------------------------
;
API_LINKISR    equ     1
API_UNLINKISR  equ     2
API_UNINSTALL  equ     3
API_LINKITV    equ     4
API_UNLINKITV  equ     5
;
; -----------------------------------------------------------------------
;   PSP relevant references
; -----------------------------------------------------------------------
;
PSPHIGH        = 00002h
PSPENV         = 0002ch
PSPCMD         = 00080h
;
; -----------------------------------------------------------------------
;   error codes
; -----------------------------------------------------------------------
;
E_MEMALLOC     equ     1
E_MEMDEALLOC   equ     2
E_INVFUNCREQ   equ     3
;
; -----------------------------------------------------------------------
;   status flags definitions
; -----------------------------------------------------------------------
;
; -----------------------------------------------------------------------
;   ISRCB -- Interrupt Service Routine Control Block
; -----------------------------------------------------------------------
;
ISRCB          struc
bIntNum        db      ?               ; interrupt number
bFlag          db      ?               ; flag
wPOfs          dw      ?               ; previous interrupt handler offset
wPSeg          dw      ?               ; previous interrupt handler segment
wNOfs          dw      ?               ; new interrupt handler offset
ISRCB          ends
;
;
; -----------------------------------------------------------------------
;   ITCB -- Interrupt Trapping Control Block
; -----------------------------------------------------------------------
;
ITCB           struc
bCallOPC       db      ?                 ; call opcode
wCallAdr       dw      ?                 ; call address
wOOfs          dw      ?                 ; original interrupt handler offset
wOSeg          dw      ?                 ; original interrupt handler segment
bInt           db      ?                 ; interrupt number
wMCOfs         dw      ?                 ; min category text offset
wSCOfs         dw      ?                 ; sub-category scan table offset
bFlags         db      ?                 ; flags
wCalls         dw      ?                 ; number of total calls
wUSOfs         dw      ?
wUSSeg         dw      ?
wBSOfs         dw      ?                 ; built in service (near ptr)
ITCB           ends
;
; -----------------------------------------------------------------------
;   ITCB flags definitions
; -----------------------------------------------------------------------
;
ITF_TRAPON  = 01h                        ; interrupt trapping enabled
ITF_STOPWT  = 02h                        ; stopwatch enabled
ITF_TRAPAF  = 04h                        ; trap also after the service
ITF_DISINF  = 08h                        ; display info
ITF_ENACAT  = 10h                        ; enable category function check
ITF_TRAPAC  = 80h                        ; trap is active
;
SCATDESCLEN = 12                         ; sub category descr. length
MCATDESCLEN = 4                          ; main category descr. length
;
; -----------------------------------------------------------------------
;   BIOS data area segment
; -----------------------------------------------------------------------
;
BIOS_SEG       segment at 40h
               org     17h
bShiftSts      db      ?                 ; state of keyboard shifts keys
BIOS_SEG       ends
;
; -----------------------------------------------------------------------
;   resident part segment declaration
; -----------------------------------------------------------------------
;
RESIDENT_GROUP group RESIDENT_TEXT,RESIDENT_DATA,RESIDENT_STACK
;
; =======================================================================
;   resident part of the program
; =======================================================================
;
RESIDENT_TEXT  segment byte public 'CODE'
               assume  cs:RESIDENT_GROUP,ds:RESIDENT_GROUP
;
;
; =======================================================================
;   IntTrapper -- takes control over an interrupt retrieving all the
;                 necessary informations
; -----------------------------------------------------------------------
;   input:   none
;
;   return:  none
; =======================================================================
;
IntTrapper     proc    far
;
TRAPTABBEG     equ     $
;
; -----------------------------------------------------------------------
;   ITCB array
; -----------------------------------------------------------------------
;
ITCB_Array     label   byte
;
ITCBE Int00 000h, b_CPUI, b_DIVBYZR, 0;  %(ITF_TRAPON + ITF_DISINF)
ITCBE Int01 001h, b_CPUI, b_SINSTEP, 0;  %(ITF_TRAPON + ITF_DISINF)
ITCBE Int02 002h, b_CPUI, b_NMSKINT, 0;  %(ITF_TRAPON + ITF_DISINF)
ITCBE Int03 003h, b_CPUI, b_BRKPOIN, 0;  %(ITF_TRAPON + ITF_DISINF)
ITCBE Int04 004h, b_CPUI, b_DIVOVER, 0;  %(ITF_TRAPON + ITF_DISINF)
ITCBE Int05 005h, b_BIOS, b_PRTSCRN, 0;  %(ITF_TRAPON + ITF_DISINF)
ITCBE Int06 006h, b_HARD, b_RESERVD, 0;  %(ITF_TRAPON + ITF_DISINF)
ITCBE Int07 007h, b_HARD, b_RESERVD, 0;  %(ITF_TRAPON + ITF_DISINF)
ITCBE Int08 008h, b_HARD, b_IRQ0TIM, 0;  %(ITF_TRAPON + ITF_DISINF)
ITCBE Int09 009h, b_HARD, b_IRQ1KBD, 0;  %(ITF_TRAPON + ITF_DISINF)
ITCBE Int0A 00Ah, b_HARD, b_IRQ2INT, 0;  %(ITF_TRAPON + ITF_DISINF)
ITCBE Int0B 00Bh, b_HARD, b_IRQ3CO2, 0;  %(ITF_TRAPON + ITF_DISINF)
ITCBE Int0C 00Ch, b_HARD, b_IRQ4CO1, 0;  %(ITF_TRAPON + ITF_DISINF)
ITCBE Int0D 00Dh, b_HARD, b_IRQ5DSK, 0;  %(ITF_TRAPON + ITF_DISINF)
ITCBE Int0E 00Eh, b_HARD, b_IRQ6FDS, 0;  %(ITF_TRAPON + ITF_DISINF)
ITCBE Int0F 00Fh, b_HARD, b_IRQ7PRT, 0;  %(ITF_TRAPON + ITF_DISINF)
ITCBE Int10 010h, b_VIDF, b_VIDFNSC, %(ITF_TRAPON + ITF_DISINF + ITF_ENACAT)
ITCBE Int11 011h, b_BIOS, b_EQUIPCK, 0;  %(ITF_TRAPON + ITF_DISINF)
ITCBE Int12 012h, b_BIOS, b_MEMSIZE, 0;  %(ITF_TRAPON + ITF_DISINF)
ITCBE Int13 013h, b_BIOS, b_UNKNOWN, 0;  %(ITF_TRAPON + ITF_DISINF)
ITCBE Int14 014h, b_BIOS, b_UNKNOWN, 0;  %(ITF_TRAPON + ITF_DISINF)
ITCBE Int15 015h, b_BIOS, b_UNKNOWN, 0;  %(ITF_TRAPON + ITF_DISINF)
ITCBE Int16 016h, b_BIOS, b_UNKNOWN, 0;  %(ITF_TRAPON + ITF_DISINF)
ITCBE Int17 017h, b_BIOS, b_UNKNOWN, 0;  %(ITF_TRAPON + ITF_DISINF)
ITCBE Int18 018h, b_BIOS, b_ROMBASI, 0;  %(ITF_TRAPON + ITF_DISINF)
ITCBE Int19 019h, b_BIOS, b_IPLVECT, 0;  %(ITF_TRAPON + ITF_DISINF)
ITCBE Int1A 01Ah, b_BIOS, b_UNKNOWN, 0;  %(ITF_TRAPON + ITF_DISINF)
ITCBE Int1B 01Bh, b_BIOS, b_CTRLBRK, 0;  %(ITF_TRAPON + ITF_DISINF)
ITCBE Int1C 01Ch, b_BIOS, b_TIMTICK, 0;  %(ITF_TRAPON + ITF_DISINF)
ITCBE Int1D 01Dh, b_BIOS, b_UNKNOWN, 0;  ; not enabled by default !! (data ptr)
ITCBE Int1E 01Eh, b_BIOS, b_UNKNOWN, 0;  ; not enabled by default !! (data ptr)
ITCBE Int1F 01Fh, b_BIOS, b_UNKNOWN, 0;  ; not enabled by default !! (data ptr)
ITCBE Int20 020h, b_GINT, b_UNKNOWN, 0;  %(ITF_TRAPON + ITF_DISINF)
ITCBE Int21 021h, b_DOSF, b_DOSFNSC, 0;  %(ITF_TRAPON + ITF_DISINF) + ITF_ENACAT)
ITCBE Int22 022h, b_GINT, b_UNKNOWN, 0;
ITCBE Int23 023h, b_GINT, b_UNKNOWN, 0;
ITCBE Int24 024h, b_GINT, b_UNKNOWN, 0;
ITCBE Int25 025h, b_GINT, b_UNKNOWN, 0;
ITCBE Int26 026h, b_GINT, b_UNKNOWN, 0;
ITCBE Int27 027h, b_GINT, b_UNKNOWN, 0;
ITCBE Int28 028h, b_GINT, b_UNKNOWN, 0;
ITCBE Int29 029h, b_GINT, b_UNKNOWN, 0;
ITCBE Int2A 02Ah, b_GINT, b_UNKNOWN, 0;
ITCBE Int2B 02Bh, b_GINT, b_UNKNOWN, 0;
ITCBE Int2C 02Ch, b_GINT, b_UNKNOWN, 0;
ITCBE Int2D 02Dh, b_GINT, b_UNKNOWN, 0;
ITCBE Int2E 02Eh, b_GINT, b_UNKNOWN, 0;
ITCBE Int2F 02Fh, b_MPLX, b_DOSMPLX, 0;  %(ITF_TRAPON + ITF_DISINF)
ITCBE Int30 030h, b_GINT, b_UNKNOWN, 0;  %(ITF_TRAPON + ITF_DISINF)
ITCBE Int31 031h, b_GINT, b_UNKNOWN, 0;  %(ITF_TRAPON + ITF_DISINF)
ITCBE Int32 032h, b_GINT, b_UNKNOWN, 0;  %(ITF_TRAPON + ITF_DISINF)
ITCBE Int33 033h, b_MOUS, b_MOUFNSC, 0;  %(ITF_TRAPON + ITF_DISINF) + ITF_ENACAT)
;
;
TRAPTABLEN     equ     $ - TRAPTABBEG
;
; -----------------------------------------------------------------------
;   common entry point for all interrupts trapping
; -----------------------------------------------------------------------
;
Trapper        label   near
               pushf
               cli
               call    SaveRegs

               pop     si                        ; si = ITCB ofs
               sub     si,3                      ; adjust to point to ITCB
               mov     ax,cs
               mov     ds,ax
               mov     ax,[si].wOOfs
               mov     [wOldIP],ax
               mov     ax,[si].wOSeg
               mov     [wOldCS],ax
;
; -----------------------------------------------------------------------
;   check for trapping enabled
; -----------------------------------------------------------------------
;
               cld                         ; assure forward string operations
               call    CheckIntTrap        ; check for trapping enabled
                jc     @@nowrap1
;
; -----------------------------------------------------------------------
;   retrieve the IP, CS and cpu flags contents
; -----------------------------------------------------------------------
;
               pop     [wIP]
               pop     [wCS]
               pop     [wFL]
               push    [wFL]
               push    [wCS]
               push    [wIP]
               sub     [wIP],2             ; adjust caller ip value
;
; -----------------------------------------------------------------------
;   display constant data the same before and after the execution of
;   the original handler
; -----------------------------------------------------------------------
;
               mov     ax,ds
               mov     es,ax               ; es = ds
               mov     di,[wLVOfs]
               mov     es,[wVSeg]
               call    ResetLineAttr
               mov     ax,ds
               mov     es,ax               ; es = ds
               mov     di,offset RESIDENT_GROUP:bOutBuf
               mov     ch,01h
               call    OutputIntInfo
               mov     ch,01h
               call    OutsRegsInfo
               mov     si,offset RESIDENT_GROUP:bOutBuf
               mov     di,[wVOfs]
               mov     es,[wVSeg]
               mov     cx,OUTBUFLEN
               rep     movsw
               add     [wVOfs],160
               cmp     [wVOfs],22*80*2
               jb      @@nowrap1
               mov     [wVOfs],2*80*2
;
; -----------------------------------------------------------------------
;   now restore caller registers and call original int. handler
; -----------------------------------------------------------------------
;
@@nowrap1:
               call    RestRegs                 ; restore caller registers
               push    cs:[wTrap]               ; save trap flag
               pushf
               call    cs:[dwJumpVect]          ; call original handler
               pop     cs:[wTrap]               ; restore trap flag
               pushf
               cli                              ; turn ints off
               call    SaveRegs                 ; save caller regs. after int
               cmp     cs:[wTrap],0             ; check for trap enabled
                je     @@nowrap2
               mov     ax,cs                    ; access data segment
               mov     ds,ax
               mov     es,ax                    ; es = ds
               call    MarkOutConst             ; mark constant data
               add     [wIP],2                  ; adjust ip (after the int instr)
               mov     di,offset RESIDENT_GROUP:bOutBuf + 20*2
               mov     ch,70h                   ; mark this line
               call    OutsRegsInfo
               mov     si,offset RESIDENT_GROUP:bOutBuf
               mov     di,[wVOfs]
               mov     es,[wVSeg]
               mov     cx,OUTBUFLEN
               rep     movsw                    ; copy buffer into video ram
               mov     ax,[wVOfs]
               mov     [wLVOfs],ax
               add     [wVOfs],160              ; update video buffer pointer
               cmp     [wVOfs],22*80*2          ; check for wrap
                jb     @@nowrap2
               mov     [wVOfs],2*80*2
@@nowrap2:
               call    RestRegs                 ; restore registers and ...
               retf    2                        ; (definitely!) back to caller
IntTrapper     endp
;
;
; =======================================================================
;   SaveRegs -- save cpu registers
; -----------------------------------------------------------------------
;   input:
;
;   return:
; =======================================================================
;
SaveRegs       proc    near
               mov     cs:[wAX],ax               ; save ax contents
               pop     ax                        ; pop ret addr of this routine
               pop     cs:[wFL]                  ; pop cpu flags
               mov     cs:[wBX],bx               ; save all registers
               mov     cs:[wCX],cx
               mov     cs:[wDX],dx
               mov     cs:[wSI],si
               mov     cs:[wDI],di
               mov     cs:[wDS],ds
               mov     cs:[wES],es
               mov     cs:[wBP],bp
               push    ax                        ; push ret addr of this routine
               mov     ax,cs:[wAX]               ; restore original ax contents
               ret
SaveRegs       endp
;
;
; =======================================================================
;   RestRegs -- restores cpu registers
; -----------------------------------------------------------------------
;   input:
;
;   return:
; =======================================================================
;

RestRegs       proc    near
               mov     ax,cs:[wAX]
               mov     bx,cs:[wBX]
               mov     cx,cs:[wCX]
               mov     dx,cs:[wDX]
               mov     si,cs:[wSI]
               mov     di,cs:[wDI]
               mov     ds,cs:[wDS]
               mov     es,cs:[wES]
               mov     bp,cs:[wBP]
               push    cs:[wFL]
               popf
               ret
RestRegs       endp
;
;
; =======================================================================
;   CheckIntTrap -- checks to see if this service must be trapped
; -----------------------------------------------------------------------
;   input:   ds:si = ITCB pointer
;
;   return:  CARRY set if must be not trapped else CARRY reset
;
;            if flag ITF_ENACAT not set
;                bx = main cat text near ptr
;            else
;                bx = sub cat text near ptr
;
;            [wTrap] also is updated (1 or 0)
; =======================================================================
;
CheckIntTrap   proc    near
               test    [si].bFlags,ITF_ENACAT ; sub-cat enabled?
                jz     @@no_subcat
               mov     bx,[si].wSCOfs         ; get sub-cat text offset
               mov     al,[bAH]               ; get sub-func code
               xor     ah,ah
               mov     cl,8
               div     cl
               mov     cl,ah
               inc     cl
               xor     ah,ah
               add     bx,ax
               mov     al,[bx]
               rcr     al,cl
                jnc    @@fast_exit
;
; -----------------------------------------------------------------------
;   get from text pointers table the address of the sub-cat text
; -----------------------------------------------------------------------
;
               mov     al,[bAH]               ; get sub-func code
               xor     ah,ah
               shl     ax,1                   ; ax = ax * 2
               mov     bx,[si].wSCOfs         ; get sub-cat text offsets table
               add     bx,FNBITMAPSIZE        ; add func codes bitmap size
               add     bx,ax                  ; bx = text descr. offset
               mov     bx,[bx]
                jmp    short @@check_disp
@@no_subcat:
               mov     bx,[si].wSCOfs          ; use default sub-cat text
               inc     [si].wCalls             ; increment total calls
@@check_disp:
               test    [si].bFlags,ITF_DISINF
                jz     @@fast_exit
               mov     [wTrap],1               ; trap enabled
               clc                             ; infos display
               ret
@@fast_exit:
               mov     [wTrap],0               ; trap disabled
               stc
               ret
CheckIntTrap   endp
;
;
; =======================================================================
;   OutputIntInfo -- produces an interrupt informations report
; -----------------------------------------------------------------------
;   input:   ds:si = ITCB ptr
;            es:di = output buffer
;            ch    = video attribute to use
;
;   return:  output buffer updated
;            di = updated (point to the next position into output buffer)
; =======================================================================
;
OutputIntInfo  proc    near
               mov     al,[si].bInt        ; get interrupt number
               mov     dx,0230h            ; two digits conversion + pad character
               cld
               call    BinToAscHex         ; convert register contents to ascii hex
               mov     ax,0fb3h
               stosw
               mov     ah,ch
               mov     si,[si].wMCOfs      ; get main category text offset
               push    cx
               mov     cx,4
@@disp_mcat:
               lodsb
               stosw
                loop   @@disp_mcat
               pop     cx
               mov     ax,0fb3h
               stosw
               mov     si,bx               ; get main category text offset
               mov     ah,ch
               mov     cx,12
@@disp_scat:
               lodsb
               stosw
                loop   @@disp_scat
               ret
OutputIntInfo  endp
;
;
; =======================================================================
;   OutsRegsInfo -- shows registers contents before to enter into the
;                   interrupt service routine
; -----------------------------------------------------------------------
;   input:   es:di = output buffer pointer
;            ch    = video attribute to use
;
;   return:  output buffer updated
; =======================================================================
;
OutsRegsInfo   proc    near
               mov     si,offset RESIDENT_GROUP:bRegsArray
               mov     bx,12           ; number of registers to display
               mov     dx,0430h        ; four digits + pad char
@@disp_loop:
               mov     ax,0fb3h
               stosw                   ; put separator bar
               lodsw                   ; get next register value
               call    BinToAscHex
               dec     bx
                jnz    @@disp_loop
               ret
OutsRegsInfo   endp
;
;
; =======================================================================
;   ResetLineAttr -- resets the output buffer
; -----------------------------------------------------------------------
;   input:  es = data segment
;
;   return: output buffer cleared
; =======================================================================
;
ResetLineAttr  proc    near
               mov     al,07h
               mov     cx,OUTBUFLEN
@@resetattr:
               inc     di
               stosb
                loop   @@resetattr

               sub     di,OUTBUFLEN*2
               mov     al,0fh
               add     di,5
               stosb
               add     di,9
               stosb
               add     di,25
               stosb
               mov     cx,11
@@restattr:
               add     di,9
               stosb
                loop   @@restattr
               ret
ResetLineAttr  endp
;
;
; =======================================================================
;   MarkOutConst -- marks the constant portion of the output buffer
; -----------------------------------------------------------------------
;   input:  es = data segment
;
;   return: output buffer cleared
; =======================================================================
;
MarkOutConst   proc    near
               mov     di,offset RESIDENT_GROUP:bOutBuf
               mov     al,70h
               inc     di
               stosb                          ; -- field 1
               inc     di
               stosb
               mov     cx,4                   ; -- field 2
               add     di,2
@@marfld2:
               inc     di
               stosb
                loop   @@marfld2
               mov     cx,12                  ; -- field 3
               add     di,2
@@marfld3:
               inc     di
               stosb
                loop   @@marfld3
               ret
MarkOutConst   endp
;
;
; =======================================================================
;  BinToAscHex -- convert a number into a ASCII HEX string
; -----------------------------------------------------------------------
;   input:   ax    = number to convert (ax = word, al = byte)
;            ch    = attribute to use for video output
;            dh    = 2 for byte conversion, 4 for word conversion
;            dl    = pad character
;            es:di = string destination buffer pointer
;
;   return:  buffer contents = ascii hexadecimal string representation
;            of the number
; -----------------------------------------------------------------------
;    regs used:    ax,cx,dx,es,di
;    regs saved:   bx,dx,cx
;    regs changed: ax,di
; =======================================================================
;
BinToAscHex    proc    near
               .push   bx,dx,cx
               xor     bh,bh
               mov     bl,dh
               xchg    dx,ax
               mov     cl,4
               cmp     bl,4
                je     @@hloop
               xchg    dh,dl
@@hloop:
               rol     dx,cl
               mov     ax,dx
               and     al,0fh
               daa
               add     al,0f0h
               adc     al,040h
               mov     ah,ch
               stosw
               dec     bx
                jnz    @@hloop
               .pop    bx,dx,cx
               ret
BinToAscHex    endp
;
;
; =======================================================================
;   f_dummy -- dummy routine
; -----------------------------------------------------------------------
;   input:   none
;
;   return:  carry flag cleared
; =======================================================================
;
f_dummy        proc    near
               xor     ax,ax
               clc
               ret
f_dummy        endp
;
;
; =======================================================================
;   f_link_ISR -- links all ISR vectors
; -----------------------------------------------------------------------
;   input:   none
;
;   return:  CARRY set if link is not possible
;            CARRY reset if ISR vectors succesfully linked
; -----------------------------------------------------------------------
;   regs saved:      ds,es
;   regs destroyed:  ax,bx,cx,si
; =======================================================================
;
f_link_ISR     proc    near
               .push   es,ds

               mov     cx,TSR_NINTH       ; get # of interr. handler to install
               mov     si,offset RESIDENT_GROUP:[ISR_list]
@@get_nex_ISR:
               lodsb                      ; al = interrupt number
               push    ax                 ; preserve ax
               mov     ah,35h             ; get interrupt vector pointer
               int     21h
               mov     [si+1],bx          ; save offset and segment ..
               mov     [si+3],es          ; of the previous handler
               pop     ax
               push    ds                 ; preserve current data segment
               mov     dx,[si+5]          ; get ISR offset
               mov     bx,RESIDENT_GROUP
               mov     ds,bx
               mov     ah,25h             ; set interrupt vector pointer
               int     21h
               pop     ds                 ; restore our data segment
               add     si,7               ; ds:si -> next in the list
                loop   @@get_nex_ISR
               clc
;
               .pop    es,ds
               ret
f_link_ISR     endp
;
;
; =======================================================================
;   f_unlink_ISR -- unlink all ISR vectors
; -----------------------------------------------------------------------
;   input:   none
;
;   return:  CARRY set if unlink is not possible
;            CARRY reset if ISR vectors succesfully unlinked
; -----------------------------------------------------------------------
;   regs saved:     ds,es
;   regs destroyed: ax,bx,cx,si
; =======================================================================
;
f_unlink_ISR   proc    near
               .push   ds                 ; save resident TRACKER segment
;
               mov     cx,TSR_NINTH       ; get # of interr. handler to install
               mov     si,offset RESIDENT_GROUP:[ISR_list]
@@put_nex_ISR:
               lodsb                      ; al = interrupt number
               push    ds                 ; save resident TRACKER data segment
               mov     dx,[si+1]          ; get offset and segment ..
               mov     ds,[si+3]          ; of the previous handler
               mov     ah,25h             ; set interrupt vector pointer
               int     21h
               pop     ds                 ; restore resident TRACKER data segment
               add     si,7               ; ds:si -> next in the list
                loop   @@put_nex_ISR
               clc
;
               .pop    ds
               ret
f_unlink_ISR   endp
;
;
; =======================================================================
;   f_uninstall -- auto-install of the resident API interface
; -----------------------------------------------------------------------
;   input:   none
;
;   return:  CARRY set if cannot uninstall
;            CARRY reset if successful uninstall
; -----------------------------------------------------------------------
;   regs saved:     es
;   regs destroyed: ax,bx
; =======================================================================
;
f_uninstall    proc    near
               push    es
               call    f_unlink_ISR      ; unlink ISRs
                jc     @@exit
               call    f_unlink_ITV      ; unlink ITVs
                jc     @@exit
               mov     bx,RESIDENT_GROUP ; access our local data
               sub     bx,10h            ; subtract PSP area size
               mov     es,bx             ; es = PSP segment of resident TRACKER
               mov     ax,es:[PSPENV]
               mov     es,ax             ; free environment area of
               mov     ah,49h            ; resident TRACKER
               int     21h
                jc     @@exit
               mov     es,bx             ; es = PSP segment of resident
               mov     ah,49h            ; free resident TRACKER memory ...
               int     21h               ; (i.e. uninstall it)
@@exit:
               pop     es
               ret
f_uninstall    endp
;
;
; =======================================================================
;   f_link_ITV -- links all interrupt traps vectors
; -----------------------------------------------------------------------
;   input:   none
;
;   return:  CARRY reset
; -----------------------------------------------------------------------
;   regs saved: ds,es
;   regs destr: ax,bx,cx,si
; =======================================================================
;
f_link_ITV     proc    near
               .push   es,ds
               mov     cx,TRAPTABLEN / size ITCB
               mov     si,offset RESIDENT_GROUP:[ITCB_Array]
               cld
               cli
@@get_next:
               test    [si].bFlags,ITF_TRAPON ; check for trap enabled
               jz      @@no_trap
               mov     al,[si].bInt             ; al = interrupt number
               push    ds                       ; save our data segment
               push    ax                       ; preserve ax
               mov     ah,35h
               int     21h
               mov     [si].wOOfs,bx            ; save offset and segment ..
               mov     [si].wOSeg,es            ; of the previous handler
               mov     dx,si
               mov     bx,RESIDENT_GROUP
               mov     ds,bx
               pop     ax
               mov     ah,25h
               int     21h
               pop     ds                 ; restore our data segment
@@no_trap:
               add     si,size ITCB       ; ds:si -> next in the list
                loop   @@get_next
               sti
               clc
               .pop    es,ds
               ret
f_link_ITV     endp
;
;
; =======================================================================
;   f_unlink_ITV -- unlinks all interrupt traps vectors
; -----------------------------------------------------------------------
;   input:   none
;
;   return:  CARRY
; -----------------------------------------------------------------------
;   regs saved:     ds,es
;   regs destroyed: ax,bx,cx,si
; =======================================================================
;
f_unlink_ITV   proc    near
               .push   ds
               mov     cx,TRAPTABLEN / size ITCB
               mov     si,offset RESIDENT_GROUP:[ITCB_Array]
               cld
               cli
@@put_next:
               test    [si].bFlags,ITF_TRAPON ; check for trap enabled
               jz      @@no_trap
               push    ds                 ; save our segment
               mov     al,[si].bInt       ; al = interrupt number
               mov     dx,[si].wOOfs      ; get offset of the orig. handler
               mov     ds,[si].wOSeg      ; get seg of the original handler
               mov     ah,25h
               int     21h
               pop     ds                 ; restore our data segment
@@no_trap:
               add     si,size ITCB       ; ds:si -> next in the list
                loop   @@put_next
               sti
               clc
               .pop    ds
               ret
f_unlink_ITV   endp
;
;
; =======================================================================
;   ISR2f_int -- INT 2Fh handler
; -----------------------------------------------------------------------
;   input:  ah = Multiplex ID
;           al = function request (0 = get installed state)
;                                 (5 = execute TRACKER function)
;           bl = TRACKER function request
;
;   return: ah = error code in case of function request 5
; -----------------------------------------------------------------------
;   regs saved:     none
;   regs destroyed: none
; =======================================================================
;
ISR2F_int      proc    far
               cmp     ah,MULTIPLEXID
                je     @@our_id
                jmp    cs:[prev_ISR2F]
@@our_id:
               cmp     ax,MPEXINTFUNC        ; check for TRACKER func req.
                jz     @@intern_func
               test    al,al                 ; check if reserved of undefined
                jnz    @@mplex_ret
               mov     al,0ffh               ; return installed ok flag
@@mplex_ret:
               iret
@@intern_func:
               .push   bx,ds
               mov     ax,RESIDENT_GROUP
               mov     ds,ax
               xor     bh,bh                 ; clear high part
               shl     bx,1                  ; adjust it to a word boundary
               add     bx,offset RESIDENT_GROUP:[fun_table] ; get func ptr table
               call    word ptr [bx]         ; do the required function
               .pop    bx,ds
                jnc    @@no_errors
               mov     ah,1                  ; signal error condition
               iret
@@no_errors:
               xor     ah,ah
               iret
ISR2F_int      endp
;
;
; =======================================================================
;   ISR08_int -- timer interrupt handler
; -----------------------------------------------------------------------
;   input:   none
;
;   return:  none
; =======================================================================
;
ISR08_int      proc    far
               pushf
               call    cs:[prev_ISR08]           ; call original int 8
               cmp     cs:[bInt08Act],0          ; are we already in this?
                jne    @@exit
               cmp     cs:[bPopUpTime],0         ; check pop-up time
                jne    @@timer_check
@@timer_check:
               push    ax
               inc     cs:[bInt08Act]            ; set int 8 active flag
;              call    CheckSystem               ; check if system ok to popup
               or      ax,ax
                jne    @@timer_dec
;              call    Main                      ; pop-up TSR
               mov     cs:[bPopUpTime],1         ;
@@timer_dec:
               dec     cs:[bPopUpTime]           ; reset pop-up time
               dec     cs:[bInt08Act]            ; reset int 8 active flag
               pop     ax
@@exit:
               iret
ISR08_int      endp
;
;
; =======================================================================
;   ISR09_int -- keyboard interrupt handler
; -----------------------------------------------------------------------
;   input:   none
;
;   return:  none
; =======================================================================
;
ISR09_int      proc    far
               pushf
               call    cs:[prev_ISR09]           ; call original int 9
               .push   ax,ds
               mov     ax,BIOS_SEG               ; access BIOS data seg
               mov     ds,ax
               assume  ds:BIOS_SEG
               mov     al,[bShiftSts]             ; get current shift status
               and     al,0fh                     ; mask lock bits
               cmp     al,cs:[bHotShift]          ; check for host-shift mask
               .pop    ax,ds
                je     @@hot_key
@@exit:
               iret
@@hot_key:
               mov     cs:[bPopUpTime],POPUPTICKS ; set timer to pop-up
               iret
ISR09_int      endp
;
RESIDENT_TEXT  ends
;
; -----------------------------------------------------------------------
;   local resident data
; -----------------------------------------------------------------------
;
RESIDENT_DATA  segment word public 'DATA'
begresdata     label   byte
;
; -----------------------------------------------------------------------
;   !!! Warning, DO NOT move or alter the following string !!!
; -----------------------------------------------------------------------
;
signature      db      'M_TRACKER v1.0' ; program signature
;
wSavSP         dw      ?                ; aux stack offset
wSavSS         dw      ?                ; aux  stack segment
wVersion       dw      0                ; DOS version number
wPSPSeg        dw      0                ; psp prefix segment address
wENVSeg        dw      0                ; program enviroment segment address
dwInDOSptr     label   dword            ; InDOS flag pointer
wInDOS_ofs     dw      0
wInDOS_seg     dw      0
dwErrModptr    label   dword            ; Error mode pointer
wErrMod_ofs    dw      0
wErrMod_seg    dw      0
wSts           dw      0                ; status flags
wTrap          dw      0                ; trap flag
wVSeg          dw      0b000h           ; video buffer segment
wVOfs          dw      2*80*2
wLVOfs         dw      2*80*2
dwJumpVect     label   dword            ; original vector far pointer
wOldIP         dw      0
wOldCS         dw      0
bHotShift      db      0eh              ; hot shifts mask (Ctrl-Alt-LShift)
bPopUpTime     db      0                ; request flag/timer
bInt08Act      db      0                ; Int 8 active flag
;
;------------------------------------------------------------------------
;   registers save array
; -----------------------------------------------------------------------
;
bRegsArray     label   byte
wCS            dw      0
wIP            dw      0
wDS            dw      0
wES            dw      0
wAX            label   word
bAL            db      0
bAH            db      0
wBX            label   word
bBL            db      0
bBH            db      0
wCX            label   word
bCL            db      0
bCH            db      0
wDX            label   word
bDL            db      0
bDH            db      0
wSI            dw      0
wDI            dw      0
wBP            dw      0
wFL            dw      0
;
; -----------------------------------------------------------------------
;   output buffer
; -----------------------------------------------------------------------
;
bOutBuf        dw      OUTBUFLEN dup (0720h)
;
;
; -----------------------------------------------------------------------
;   DOS related sub-function bitmap table & text description ptrs table
; -----------------------------------------------------------------------
;   * = undocumented   + = supported under OS/2 compat.box
; -----------------------------------------------------------------------
;
b_DOSFNSC      label   byte
;
; -------------------- FEDCBA9876543210FEDCBA9876543210 --
;
               dd      11111111111111111111011111111111b  ; 00-1F
               dd      11111111111111111111111111111111b  ; 20-3F
               dd      11111111111111111111111111111111b  ; 40-5F
               dd      11111111111111111111111111111111b  ; 60-7F
               dd      11111111111111111111111111111111b  ; 80-9F
               dd      11111111111111111111111111111111b  ; A0-BF
               dd      11111111111111111111111111111111b  ; C0-DF
               dd      11111111111111111111111111111111b  ; E0-FF
b_DOSFNTP      label   word
               .data_ofs b_DOSFN00  ; 00h
               .data_ofs b_DOSFN01  ; 01h
               .data_ofs b_DOSFN02  ; 02h
               .data_ofs b_DOSFN03  ; 03h
               .data_ofs b_DOSFN04  ; 04h
               .data_ofs b_DOSFN05  ; 05h
               .data_ofs b_DOSFN06  ; 06h
               .data_ofs b_DOSFN07  ; 07h
               .data_ofs b_DOSFN08  ; 08h
               .data_ofs b_DOSFN09  ; 09h
               .data_ofs b_DOSFN0A  ; 0Ah
               .data_ofs b_DOSFN0B  ; 0Bh
               .data_ofs b_DOSFN0C  ; 0Ch
               .data_ofs b_DOSFN0D  ; 0Dh
               .data_ofs b_DOSFN0E  ; 0Eh
               .data_ofs b_DOSFN0F  ; 0Fh
               .data_ofs b_DOSFN10  ; 10h
               .data_ofs b_DOSFN11  ; 11h
               .data_ofs b_DOSFN12  ; 12h
               .data_ofs b_DOSFN13  ; 13h
               .data_ofs b_DOSFN14  ; 14h
               .data_ofs b_DOSFN15  ; 15h
               .data_ofs b_DOSFN16  ; 16h
               .data_ofs b_DOSFN17  ; 17h
               .data_ofs b_UNKNOWN  ; 18h
               .data_ofs b_DOSFN19  ; 19h
               .data_ofs b_DOSFN1A  ; 1Ah
               .data_ofs b_DOSFN1B  ; 1Bh
               .data_ofs b_DOSFN1C  ; 1Ch
               .data_ofs b_UNKNOWN  ; 1Dh
               .data_ofs b_UNKNOWN  ; 1Eh
               .data_ofs b_DOSFN1F  ; 1Fh
               .data_ofs b_INTERNL  ; 20h
               .data_ofs b_DOSFN21  ; 21h
               .data_ofs b_DOSFN22  ; 22h
               .data_ofs b_DOSFN23  ; 23h
               .data_ofs b_DOSFN24  ; 24h
               .data_ofs b_DOSFN25  ; 25h
               .data_ofs b_DOSFN26  ; 26h
               .data_ofs b_DOSFN27  ; 27h
               .data_ofs b_DOSFN28  ; 28h
               .data_ofs b_DOSFN29  ; 29h
               .data_ofs b_DOSFN2A  ; 2Ah
               .data_ofs b_DOSFN2B  ; 2Bh
               .data_ofs b_DOSFN2C  ; 2Ch
               .data_ofs b_DOSFN2D  ; 2Dh
               .data_ofs b_DOSFN2E  ; 2Eh
               .data_ofs b_DOSFN2F  ; 2Fh
               .data_ofs b_DOSFN30  ; 30h
               .data_ofs b_DOSFN31  ; 31h
               .data_ofs b_DOSFN32  ; 32h
               .data_ofs b_DOSFN33  ; 33h
               .data_ofs b_DOSFN34  ; 34h
               .data_ofs b_DOSFN35  ; 35h
               .data_ofs b_DOSFN36  ; 36h
               .data_ofs b_DOSFN37  ; 37h
               .data_ofs b_DOSFN38  ; 38h
               .data_ofs b_DOSFN39  ; 39h
               .data_ofs b_DOSFN3A  ; 3Ah
               .data_ofs b_DOSFN3B  ; 3Bh
               .data_ofs b_DOSFN3C  ; 3Ch
               .data_ofs b_DOSFN3D  ; 3Dh
               .data_ofs b_DOSFN3E  ; 3Eh
               .data_ofs b_DOSFN3F  ; 3Fh
               .data_ofs b_DOSFN40  ; 40h
               .data_ofs b_DOSFN41  ; 41h
               .data_ofs b_DOSFN42  ; 42h
               .data_ofs b_DOSFN43  ; 43h
               .data_ofs b_DOSFN44  ; 44h
               .data_ofs b_DOSFN45  ; 45h
               .data_ofs b_DOSFN46  ; 46h
               .data_ofs b_DOSFN47  ; 47h
               .data_ofs b_DOSFN48  ; 48h
               .data_ofs b_DOSFN49  ; 49h
               .data_ofs b_DOSFN4A  ; 4Ah
               .data_ofs b_DOSFN4B  ; 4Bh
               .data_ofs b_DOSFN4C  ; 4Ch
               .data_ofs b_DOSFN4D  ; 4Dh
               .data_ofs b_DOSFN4E  ; 4Eh
               .data_ofs b_DOSFN4F  ; 4Fh
               .data_ofs b_DOSFN50  ; 50h
               .data_ofs b_DOSFN51  ; 51h
               .data_ofs b_DOSFN52  ; 52h
               .data_ofs b_DOSFN53  ; 53h
               .data_ofs b_DOSFN54  ; 54h
               .data_ofs b_DOSFN55  ; 55h
               .data_ofs b_DOSFN56  ; 56h
               .data_ofs b_DOSFN57  ; 57h
               .data_ofs b_DOSFN58  ; 58h
               .data_ofs b_DOSFN59  ; 59h
               .data_ofs b_DOSFN5A  ; 5Ah
               .data_ofs b_DOSFN5B  ; 5Bh
               .data_ofs b_DOSFN5C  ; 5Ch
               .data_ofs b_DOSFN5D  ; 5Dh
               .data_ofs b_DOSFN5E  ; 5Eh
               .data_ofs b_DOSFN5F  ; 5Fh
               .data_ofs b_DOSFN60  ; 60h
               .data_ofs b_INTERNL  ; 61h
               .data_ofs b_DOSFN62  ; 62h
               .data_ofs b_DOSFN63  ; 63h
               .data_ofs b_INTERNL  ; 64h
               .data_ofs b_DOSFN65  ; 65h
               .data_ofs b_DOSFN66  ; 66h
               .data_ofs b_DOSFN67  ; 67h
               .data_ofs b_DOSFN68  ; 68h
               .data_ofs b_DOSFN69  ; 69h
               .data_ofs b_UNKNOWN  ; 6Ah
               .data_ofs b_UNKNOWN  ; 6Bh
               .data_ofs b_DOSFN6C  ; 6Ch
               .data_ofs b_RESERVD  ; 6Dh
               .data_ofs b_RESERVD  ; 6Eh
               .data_ofs b_RESERVD  ; 6Fh
               .data_ofs b_RESERVD  ; 70h
               .data_ofs b_RESERVD  ; 71h
               .data_ofs b_RESERVD  ; 72h
               .data_ofs b_RESERVD  ; 73h
               .data_ofs b_RESERVD  ; 74h
               .data_ofs b_RESERVD  ; 75h
               .data_ofs b_RESERVD  ; 76h
               .data_ofs b_RESERVD  ; 77h
               .data_ofs b_RESERVD  ; 78h
               .data_ofs b_RESERVD  ; 79h
               .data_ofs b_RESERVD  ; 7Ah
               .data_ofs b_RESERVD  ; 7Bh
               .data_ofs b_RESERVD  ; 7Ch
               .data_ofs b_RESERVD  ; 7Dh
               .data_ofs b_RESERVD  ; 7Eh
               .data_ofs b_RESERVD  ; 7Fh
               .data_ofs b_RESERVD  ; 80h
               .data_ofs b_RESERVD  ; 81h
               .data_ofs b_RESERVD  ; 82h
               .data_ofs b_RESERVD  ; 83h
               .data_ofs b_RESERVD  ; 84h
               .data_ofs b_RESERVD  ; 85h
               .data_ofs b_RESERVD  ; 86h
               .data_ofs b_RESERVD  ; 87h
               .data_ofs b_RESERVD  ; 88h
               .data_ofs b_RESERVD  ; 89h
               .data_ofs b_RESERVD  ; 8Ah
               .data_ofs b_RESERVD  ; 8Bh
               .data_ofs b_RESERVD  ; 8Ch
               .data_ofs b_RESERVD  ; 8Dh
               .data_ofs b_RESERVD  ; 8Eh
               .data_ofs b_RESERVD  ; 8Fh
               .data_ofs b_RESERVD  ; 90h
               .data_ofs b_RESERVD  ; 91h
               .data_ofs b_RESERVD  ; 92h
               .data_ofs b_RESERVD  ; 93h
               .data_ofs b_RESERVD  ; 94h
               .data_ofs b_RESERVD  ; 95h
               .data_ofs b_RESERVD  ; 96h
               .data_ofs b_RESERVD  ; 97h
               .data_ofs b_RESERVD  ; 98h
               .data_ofs b_RESERVD  ; 99h
               .data_ofs b_RESERVD  ; 9Ah
               .data_ofs b_RESERVD  ; 9Bh
               .data_ofs b_RESERVD  ; 9Ch
               .data_ofs b_RESERVD  ; 9Dh
               .data_ofs b_RESERVD  ; 9Eh
               .data_ofs b_RESERVD  ; 9Fh
               .data_ofs b_RESERVD  ; A0h
               .data_ofs b_RESERVD  ; A1h
               .data_ofs b_RESERVD  ; A2h
               .data_ofs b_RESERVD  ; A3h
               .data_ofs b_RESERVD  ; A4h
               .data_ofs b_RESERVD  ; A5h
               .data_ofs b_RESERVD  ; A6h
               .data_ofs b_RESERVD  ; A7h
               .data_ofs b_RESERVD  ; A8h
               .data_ofs b_RESERVD  ; A9h
               .data_ofs b_RESERVD  ; AAh
               .data_ofs b_RESERVD  ; ABh
               .data_ofs b_RESERVD  ; ACh
               .data_ofs b_RESERVD  ; ADh
               .data_ofs b_RESERVD  ; AEh
               .data_ofs b_RESERVD  ; AFh
               .data_ofs b_RESERVD  ; B0h
               .data_ofs b_RESERVD  ; B1h
               .data_ofs b_RESERVD  ; B2h
               .data_ofs b_RESERVD  ; B3h
               .data_ofs b_RESERVD  ; B4h
               .data_ofs b_RESERVD  ; B5h
               .data_ofs b_RESERVD  ; B6h
               .data_ofs b_RESERVD  ; B7h
               .data_ofs b_RESERVD  ; B8h
               .data_ofs b_RESERVD  ; B9h
               .data_ofs b_RESERVD  ; BAh
               .data_ofs b_RESERVD  ; BBh
               .data_ofs b_RESERVD  ; BCh
               .data_ofs b_RESERVD  ; BDh
               .data_ofs b_RESERVD  ; BEh
               .data_ofs b_RESERVD  ; BFh
               .data_ofs b_RESERVD  ; C0h
               .data_ofs b_RESERVD  ; C1h
               .data_ofs b_RESERVD  ; C2h
               .data_ofs b_RESERVD  ; C3h
               .data_ofs b_RESERVD  ; C4h
               .data_ofs b_RESERVD  ; C5h
               .data_ofs b_RESERVD  ; C6h
               .data_ofs b_RESERVD  ; C7h
               .data_ofs b_RESERVD  ; C8h
               .data_ofs b_RESERVD  ; C9h
               .data_ofs b_RESERVD  ; CAh
               .data_ofs b_RESERVD  ; CBh
               .data_ofs b_RESERVD  ; CCh
               .data_ofs b_RESERVD  ; CDh
               .data_ofs b_RESERVD  ; CEh
               .data_ofs b_RESERVD  ; CFh
               .data_ofs b_RESERVD  ; D0h
               .data_ofs b_RESERVD  ; D1h
               .data_ofs b_RESERVD  ; D2h
               .data_ofs b_RESERVD  ; D3h
               .data_ofs b_RESERVD  ; D4h
               .data_ofs b_RESERVD  ; D5h
               .data_ofs b_RESERVD  ; D6h
               .data_ofs b_RESERVD  ; D7h
               .data_ofs b_RESERVD  ; D8h
               .data_ofs b_RESERVD  ; D9h
               .data_ofs b_RESERVD  ; DAh
               .data_ofs b_RESERVD  ; DBh
               .data_ofs b_RESERVD  ; DCh
               .data_ofs b_RESERVD  ; DDh
               .data_ofs b_RESERVD  ; DEh
               .data_ofs b_RESERVD  ; DFh
               .data_ofs b_RESERVD  ; E0h
               .data_ofs b_RESERVD  ; E1h
               .data_ofs b_RESERVD  ; E2h
               .data_ofs b_RESERVD  ; E3h
               .data_ofs b_RESERVD  ; E4h
               .data_ofs b_RESERVD  ; E5h
               .data_ofs b_RESERVD  ; E6h
               .data_ofs b_RESERVD  ; E7h
               .data_ofs b_RESERVD  ; E8h
               .data_ofs b_RESERVD  ; E9h
               .data_ofs b_RESERVD  ; EAh
               .data_ofs b_RESERVD  ; EBh
               .data_ofs b_RESERVD  ; ECh
               .data_ofs b_RESERVD  ; EDh
               .data_ofs b_RESERVD  ; EEh
               .data_ofs b_RESERVD  ; EFh
               .data_ofs b_RESERVD  ; F0h
               .data_ofs b_RESERVD  ; F1h
               .data_ofs b_RESERVD  ; F2h
               .data_ofs b_RESERVD  ; F3h
               .data_ofs b_RESERVD  ; F4h
               .data_ofs b_RESERVD  ; F5h
               .data_ofs b_RESERVD  ; F6h
               .data_ofs b_RESERVD  ; F7h
               .data_ofs b_RESERVD  ; F8h
               .data_ofs b_RESERVD  ; F9h
               .data_ofs b_RESERVD  ; FAh
               .data_ofs b_RESERVD  ; FBh
               .data_ofs b_RESERVD  ; FCh
               .data_ofs b_RESERVD  ; FDh
               .data_ofs b_RESERVD  ; FEh
               .data_ofs b_RESERVD  ; FFh
;
; -----------------------------------------------------------------------
;   BIOS Video related sub-function bitmap table & text descr. ptrs table
; -----------------------------------------------------------------------
;
b_VIDFNSC      label   byte
;
; -------------------- FEDCBA9876543210FEDCBA9876543210 --
;
               dd      00011100001111111111111111111111b  ; 00-1F
               dd      00000000000000000000000000000000b  ; 20-3F
               dd      00000000000000000000000000000000b  ; 40-5F
               dd      00000000000000000000000000000000b  ; 60-7F
               dd      00000000000000000000000000000000b  ; 80-9F
               dd      00000000000000000000000000000000b  ; A0-BF
               dd      00000000000000000000000000000000b  ; C0-DF
               dd      00000000000000000000000000000000b  ; E0-FF
b_VIDFNTP      label   word
               .data_ofs b_VIDFN00  ; 00h
               .data_ofs b_VIDFN01  ; 01h
               .data_ofs b_VIDFN02  ; 02h
               .data_ofs b_VIDFN03  ; 03h
               .data_ofs b_VIDFN04  ; 04h
               .data_ofs b_VIDFN05  ; 05h
               .data_ofs b_VIDFN06  ; 06h
               .data_ofs b_VIDFN07  ; 07h
               .data_ofs b_VIDFN08  ; 08h
               .data_ofs b_VIDFN09  ; 09h
               .data_ofs b_VIDFN0A  ; 0Ah
               .data_ofs b_VIDFN0B  ; 0Bh
               .data_ofs b_VIDFN0C  ; 0Ch
               .data_ofs b_VIDFN0D  ; 0Dh
               .data_ofs b_VIDFN0E  ; 0Eh
               .data_ofs b_VIDFN0F  ; 0Fh
               .data_ofs b_VIDFN10  ; 10h
               .data_ofs b_VIDFN11  ; 11h
               .data_ofs b_VIDFN12  ; 12h
               .data_ofs b_VIDFN13  ; 13h
               .data_ofs b_VIDFN14  ; 14h
               .data_ofs b_VIDFN15  ; 15h
               .data_ofs b_RESERVD  ; 16h
               .data_ofs b_RESERVD  ; 17h
               .data_ofs b_RESERVD  ; 18h
               .data_ofs b_RESERVD  ; 19h
               .data_ofs b_VIDFN1A  ; 1Ah
               .data_ofs b_VIDFN1B  ; 1Bh
               .data_ofs b_VIDFN1C  ; 1Ch
               .data_ofs b_RESERVD  ; 1Dh
               .data_ofs b_RESERVD  ; 1Eh
               .data_ofs b_RESERVD  ; 1Fh
               .data_ofs b_RESERVD  ; 20h
               .data_ofs b_RESERVD  ; 21h
               .data_ofs b_RESERVD  ; 22h
               .data_ofs b_RESERVD  ; 23h
               .data_ofs b_RESERVD  ; 24h
               .data_ofs b_RESERVD  ; 25h
               .data_ofs b_RESERVD  ; 26h
               .data_ofs b_RESERVD  ; 27h
               .data_ofs b_RESERVD  ; 28h
               .data_ofs b_RESERVD  ; 29h
               .data_ofs b_RESERVD  ; 2Ah
               .data_ofs b_RESERVD  ; 2Bh
               .data_ofs b_RESERVD  ; 2Ch
               .data_ofs b_RESERVD  ; 2Dh
               .data_ofs b_RESERVD  ; 2Eh
               .data_ofs b_RESERVD  ; 2Fh
               .data_ofs b_RESERVD  ; 30h
               .data_ofs b_RESERVD  ; 31h
               .data_ofs b_RESERVD  ; 32h
               .data_ofs b_RESERVD  ; 33h
               .data_ofs b_RESERVD  ; 34h
               .data_ofs b_RESERVD  ; 35h
               .data_ofs b_RESERVD  ; 36h
               .data_ofs b_RESERVD  ; 37h
               .data_ofs b_RESERVD  ; 38h
               .data_ofs b_RESERVD  ; 39h
               .data_ofs b_RESERVD  ; 3Ah
               .data_ofs b_RESERVD  ; 3Bh
               .data_ofs b_RESERVD  ; 3Ch
               .data_ofs b_RESERVD  ; 3Dh
               .data_ofs b_RESERVD  ; 3Eh
               .data_ofs b_RESERVD  ; 3Fh
               .data_ofs b_RESERVD  ; 40h
               .data_ofs b_RESERVD  ; 41h
               .data_ofs b_RESERVD  ; 42h
               .data_ofs b_RESERVD  ; 43h
               .data_ofs b_RESERVD  ; 44h
               .data_ofs b_RESERVD  ; 45h
               .data_ofs b_RESERVD  ; 46h
               .data_ofs b_RESERVD  ; 47h
               .data_ofs b_RESERVD  ; 48h
               .data_ofs b_RESERVD  ; 49h
               .data_ofs b_RESERVD  ; 4Ah
               .data_ofs b_RESERVD  ; 4Bh
               .data_ofs b_RESERVD  ; 4Ch
               .data_ofs b_RESERVD  ; 4Dh
               .data_ofs b_RESERVD  ; 4Eh
               .data_ofs b_RESERVD  ; 4Fh
               .data_ofs b_RESERVD  ; 50h
               .data_ofs b_RESERVD  ; 51h
               .data_ofs b_RESERVD  ; 52h
               .data_ofs b_RESERVD  ; 53h
               .data_ofs b_RESERVD  ; 54h
               .data_ofs b_RESERVD  ; 55h
               .data_ofs b_RESERVD  ; 56h
               .data_ofs b_RESERVD  ; 57h
               .data_ofs b_RESERVD  ; 58h
               .data_ofs b_RESERVD  ; 59h
               .data_ofs b_RESERVD  ; 5Ah
               .data_ofs b_RESERVD  ; 5Bh
               .data_ofs b_RESERVD  ; 5Ch
               .data_ofs b_RESERVD  ; 5Dh
               .data_ofs b_RESERVD  ; 5Eh
               .data_ofs b_RESERVD  ; 5Fh
               .data_ofs b_RESERVD  ; 60h
               .data_ofs b_RESERVD  ; 61h
               .data_ofs b_RESERVD  ; 62h
               .data_ofs b_RESERVD  ; 63h
               .data_ofs b_RESERVD  ; 64h
               .data_ofs b_RESERVD  ; 65h
               .data_ofs b_RESERVD  ; 66h
               .data_ofs b_RESERVD  ; 67h
               .data_ofs b_RESERVD  ; 68h
               .data_ofs b_RESERVD  ; 69h
               .data_ofs b_RESERVD  ; 6Ah
               .data_ofs b_RESERVD  ; 6Bh
               .data_ofs b_RESERVD  ; 6Ch
               .data_ofs b_RESERVD  ; 6Dh
               .data_ofs b_RESERVD  ; 6Eh
               .data_ofs b_RESERVD  ; 6Fh
               .data_ofs b_RESERVD  ; 70h
               .data_ofs b_RESERVD  ; 71h
               .data_ofs b_RESERVD  ; 72h
               .data_ofs b_RESERVD  ; 73h
               .data_ofs b_RESERVD  ; 74h
               .data_ofs b_RESERVD  ; 75h
               .data_ofs b_RESERVD  ; 76h
               .data_ofs b_RESERVD  ; 77h
               .data_ofs b_RESERVD  ; 78h
               .data_ofs b_RESERVD  ; 79h
               .data_ofs b_RESERVD  ; 7Ah
               .data_ofs b_RESERVD  ; 7Bh
               .data_ofs b_RESERVD  ; 7Ch
               .data_ofs b_RESERVD  ; 7Dh
               .data_ofs b_RESERVD  ; 7Eh
               .data_ofs b_RESERVD  ; 7Fh
               .data_ofs b_RESERVD  ; 80h
               .data_ofs b_RESERVD  ; 81h
               .data_ofs b_RESERVD  ; 82h
               .data_ofs b_RESERVD  ; 83h
               .data_ofs b_RESERVD  ; 84h
               .data_ofs b_RESERVD  ; 85h
               .data_ofs b_RESERVD  ; 86h
               .data_ofs b_RESERVD  ; 87h
               .data_ofs b_RESERVD  ; 88h
               .data_ofs b_RESERVD  ; 89h
               .data_ofs b_RESERVD  ; 8Ah
               .data_ofs b_RESERVD  ; 8Bh
               .data_ofs b_RESERVD  ; 8Ch
               .data_ofs b_RESERVD  ; 8Dh
               .data_ofs b_RESERVD  ; 8Eh
               .data_ofs b_RESERVD  ; 8Fh
               .data_ofs b_RESERVD  ; 90h
               .data_ofs b_RESERVD  ; 91h
               .data_ofs b_RESERVD  ; 92h
               .data_ofs b_RESERVD  ; 93h
               .data_ofs b_RESERVD  ; 94h
               .data_ofs b_RESERVD  ; 95h
               .data_ofs b_RESERVD  ; 96h
               .data_ofs b_RESERVD  ; 97h
               .data_ofs b_RESERVD  ; 98h
               .data_ofs b_RESERVD  ; 99h
               .data_ofs b_RESERVD  ; 9Ah
               .data_ofs b_RESERVD  ; 9Bh
               .data_ofs b_RESERVD  ; 9Ch
               .data_ofs b_RESERVD  ; 9Dh
               .data_ofs b_RESERVD  ; 9Eh
               .data_ofs b_RESERVD  ; 9Fh
               .data_ofs b_RESERVD  ; A0h
               .data_ofs b_RESERVD  ; A1h
               .data_ofs b_RESERVD  ; A2h
               .data_ofs b_RESERVD  ; A3h
               .data_ofs b_RESERVD  ; A4h
               .data_ofs b_RESERVD  ; A5h
               .data_ofs b_RESERVD  ; A6h
               .data_ofs b_RESERVD  ; A7h
               .data_ofs b_RESERVD  ; A8h
               .data_ofs b_RESERVD  ; A9h
               .data_ofs b_RESERVD  ; AAh
               .data_ofs b_RESERVD  ; ABh
               .data_ofs b_RESERVD  ; ACh
               .data_ofs b_RESERVD  ; ADh
               .data_ofs b_RESERVD  ; AEh
               .data_ofs b_RESERVD  ; AFh
               .data_ofs b_RESERVD  ; B0h
               .data_ofs b_RESERVD  ; B1h
               .data_ofs b_RESERVD  ; B2h
               .data_ofs b_RESERVD  ; B3h
               .data_ofs b_RESERVD  ; B4h
               .data_ofs b_RESERVD  ; B5h
               .data_ofs b_RESERVD  ; B6h
               .data_ofs b_RESERVD  ; B7h
               .data_ofs b_RESERVD  ; B8h
               .data_ofs b_RESERVD  ; B9h
               .data_ofs b_RESERVD  ; BAh
               .data_ofs b_RESERVD  ; BBh
               .data_ofs b_RESERVD  ; BCh
               .data_ofs b_RESERVD  ; BDh
               .data_ofs b_RESERVD  ; BEh
               .data_ofs b_RESERVD  ; BFh
               .data_ofs b_RESERVD  ; C0h
               .data_ofs b_RESERVD  ; C1h
               .data_ofs b_RESERVD  ; C2h
               .data_ofs b_RESERVD  ; C3h
               .data_ofs b_RESERVD  ; C4h
               .data_ofs b_RESERVD  ; C5h
               .data_ofs b_RESERVD  ; C6h
               .data_ofs b_RESERVD  ; C7h
               .data_ofs b_RESERVD  ; C8h
               .data_ofs b_RESERVD  ; C9h
               .data_ofs b_RESERVD  ; CAh
               .data_ofs b_RESERVD  ; CBh
               .data_ofs b_RESERVD  ; CCh
               .data_ofs b_RESERVD  ; CDh
               .data_ofs b_RESERVD  ; CEh
               .data_ofs b_RESERVD  ; CFh
               .data_ofs b_RESERVD  ; D0h
               .data_ofs b_RESERVD  ; D1h
               .data_ofs b_RESERVD  ; D2h
               .data_ofs b_RESERVD  ; D3h
               .data_ofs b_RESERVD  ; D4h
               .data_ofs b_RESERVD  ; D5h
               .data_ofs b_RESERVD  ; D6h
               .data_ofs b_RESERVD  ; D7h
               .data_ofs b_RESERVD  ; D8h
               .data_ofs b_RESERVD  ; D9h
               .data_ofs b_RESERVD  ; DAh
               .data_ofs b_RESERVD  ; DBh
               .data_ofs b_RESERVD  ; DCh
               .data_ofs b_RESERVD  ; DDh
               .data_ofs b_RESERVD  ; DEh
               .data_ofs b_RESERVD  ; DFh
               .data_ofs b_RESERVD  ; E0h
               .data_ofs b_RESERVD  ; E1h
               .data_ofs b_RESERVD  ; E2h
               .data_ofs b_RESERVD  ; E3h
               .data_ofs b_RESERVD  ; E4h
               .data_ofs b_RESERVD  ; E5h
               .data_ofs b_RESERVD  ; E6h
               .data_ofs b_RESERVD  ; E7h
               .data_ofs b_RESERVD  ; E8h
               .data_ofs b_RESERVD  ; E9h
               .data_ofs b_RESERVD  ; EAh
               .data_ofs b_RESERVD  ; EBh
               .data_ofs b_RESERVD  ; ECh
               .data_ofs b_RESERVD  ; EDh
               .data_ofs b_RESERVD  ; EEh
               .data_ofs b_RESERVD  ; EFh
               .data_ofs b_RESERVD  ; F0h
               .data_ofs b_RESERVD  ; F1h
               .data_ofs b_RESERVD  ; F2h
               .data_ofs b_RESERVD  ; F3h
               .data_ofs b_RESERVD  ; F4h
               .data_ofs b_RESERVD  ; F5h
               .data_ofs b_RESERVD  ; F6h
               .data_ofs b_RESERVD  ; F7h
               .data_ofs b_RESERVD  ; F8h
               .data_ofs b_RESERVD  ; F9h
               .data_ofs b_RESERVD  ; FAh
               .data_ofs b_RESERVD  ; FBh
               .data_ofs b_RESERVD  ; FCh
               .data_ofs b_RESERVD  ; FDh
               .data_ofs b_RESERVD  ; FEh
               .data_ofs b_RESERVD  ; FFh
;
; -----------------------------------------------------------------------
;   Mouse related sub-function bitmap table & text descr. ptrs table
; -----------------------------------------------------------------------
;
b_MOUFNSC      label   byte
;
; -------------------- FEDCBA9876543210FEDCBA9876543210 --
;
               dd      00000000000000011111111111111111b  ; 00-1F
               dd      00000000000000000000000000000000b  ; 20-3F
               dd      00000000000000000000000000000000b  ; 40-5F
               dd      00000000000000000000000000000000b  ; 60-7F
               dd      00000000000000000000000000000000b  ; 80-9F
               dd      00000000000000000000000000000000b  ; A0-BF
               dd      00000000000000000000000000000000b  ; C0-DF
               dd      00000000000000000000000000000000b  ; E0-FF
b_MOUFNTP      label   word
               .data_ofs b_VIDFN00  ; 00h
               .data_ofs b_VIDFN01  ; 01h
               .data_ofs b_VIDFN02  ; 02h
               .data_ofs b_VIDFN03  ; 03h
               .data_ofs b_VIDFN04  ; 04h
               .data_ofs b_VIDFN05  ; 05h
               .data_ofs b_VIDFN06  ; 06h
               .data_ofs b_VIDFN07  ; 07h
               .data_ofs b_VIDFN08  ; 08h
               .data_ofs b_VIDFN09  ; 09h
               .data_ofs b_VIDFN0A  ; 0Ah
               .data_ofs b_VIDFN0B  ; 0Bh
               .data_ofs b_VIDFN0C  ; 0Ch
               .data_ofs b_VIDFN0D  ; 0Dh
               .data_ofs b_VIDFN0E  ; 0Eh
               .data_ofs b_VIDFN0F  ; 0Fh
               .data_ofs b_VIDFN10  ; 10h
;
; -----------------------------------------------------------------------
;   Interrupt categories descriptions
; -----------------------------------------------------------------------
;
bIntCateg      label   byte
b_GINT         db      'INT '   ; (xx) generic entry
b_CPUI         db      'CPU '   ; (00-04) cpu generated interrupt
b_HARD         db      'HARD'   ; (06-0F) hardware interrupt
b_VIDF         db      'VIDF'   ; BIOS Video services
b_BIOS         db      'BIOS'   ; (11-1F, 40-5F) BIOS services
b_DOSI         db      'DOSI'   ; (20-3F) DOS interrupts
b_DOSF         db      'DOSF'   ; (21) DOS functions
b_MPLX         db      'MPLX'   ; (2F) DOS multiplex
b_MOUS         db      'MOUS'   ; (33) Mouse interface
b_NBIO         db      'NBIO'   ; (5C) Netbios interface
b_USER         db      'USER'   ; (60-66) User program interrupts
b_EMSF         db      'EMS '   ; (67) EMS services
b_XMSS         db      'XMS '   ; (??) XMS services
b_RIBM         db      'IBMr'   ; (68-7F, F1-FF) Reserved by IBM
b_BASI         db      'BASI'   ; (86-F0) used by basic interp. when running
;
; -----------------------------------------------------------------------
;   Interrupt main/sub-categories descriptions
; -----------------------------------------------------------------------
;
bSubCateg      label   byte
b_RESERVD db '* reserved *' ; reserved function
b_INTERNL db '* internal *' ; internal use
b_UNKNOWN db '* unknown  *' ;  (unknown services)

b_DIVBYZR db 'Divid.by 0  ' ;  0
b_SINSTEP db 'Singl.step  ' ;  1
b_NMSKINT db 'NoMask Int  ' ;  2
b_BRKPOIN db 'Brk Point   ' ;  3
b_DIVOVER db 'Div.Overfl  ' ;  4
b_PRTSCRN db 'Print Scr   ' ;  5
                            ;  6 - irq 6 (diskette)
                            ;  7 - irq 7 (printer)
b_IRQ0TIM db 'IRQ0 Timer  ' ;  8
b_IRQ1KBD db 'IRQ1 Keyb.  ' ;  9
b_IRQ2INT db 'IRQ2        ' ;  A
b_IRQ3CO2 db 'IRQ3 COM 2  ' ;  B
b_IRQ4CO1 db 'IRQ4 COM 1  ' ;  C
b_IRQ5DSK db 'IRQ5 HDisk  ' ;  D
b_IRQ6FDS db 'IRQ6 FDisk  ' ;  E
b_IRQ7PRT db 'IRQ7 Prnt   ' ;  F
                            ; 10 - video interface
b_EQUIPCK db 'EquipCheck  ' ; 11
b_MEMSIZE db 'MemorySize  ' ; 12
                            ; 13 - disk interface
                            ; 14 - bios communication
                            ; 15 - bios system services
                            ; 16 - bios keyboard i/o
                            ; 17 -
b_ROMBASI db 'Rom Basic   ' ; 18
b_IPLVECT db 'IPL code    ' ; 19
                            ; 1A - time of day
b_CTRLBRK db 'Ctrl Break  ' ; 1B
b_TIMTICK db 'Timer Tick  ' ; 1C
                            ; 1D - video init.          (DWORD ptr)
                            ; 1E - diskette contr parms (DWORD ptr)
                            ; 1F - graph.chars extens.  (DWORD ptr)
b_DOSTERM db 'DOS Termin. ' ; 20
                            ; 21 - DOS functions
                            ; 22 - DOS functions
                            ; 23 - DOS functions
                            ; 24 - DOS functions
                            ; 25 - DOS functions
                            ; 26 - DOS functions
                            ; 27 - DOS functions
                            ; 28 - DOS functions
                            ; 29 - DOS functions
                            ; 2A - DOS functions
                            ; 2B - DOS functions
                            ; 2C - DOS functions
                            ; 2D - DOS functions
                            ; 2E - DOS functions
b_DOSMPLX db 'MPlex Int   ' ; 2F - Multiplex interrupt
                            ; 30
                            ; 31
                            ; 32
b_MOUSFNC db 'Mouse Func  ' ; 33 - mouse functions

;
; -----------------------------------------------------------------------
;   BIOS Video functions realted text descriptions
; -----------------------------------------------------------------------
;
b_VIDFN00      db      'Set Mode    ' ; 00h Determine or Set Video State
b_VIDFN01      db      'Set CursType' ; 01h Set Cursor Type
b_VIDFN02      db      'Set Curs Pos' ; 02h Set Cursor Position
b_VIDFN03      db      'Get Curs Pos' ; 03h Read Cursor Position
b_VIDFN04      db      'Read L. Pen ' ; 04h Read Light Pen
b_VIDFN05      db      'Sel Act Page' ; 05h Select Active Page
b_VIDFN06      db      'Scroll Up   ' ; 06h Scroll Page Up
b_VIDFN07      db      'Scroll Dn   ' ; 07h Scroll Page Down
b_VIDFN08      db      'Read Chr Att' ; 08h Read Character Attribute
b_VIDFN09      db      'Write ChrAtt' ; 09h Write Character and Attribute
b_VIDFN0A      db      'Write Char  ' ; 0Ah Write Character
b_VIDFN0B      db      'Set ColorPal' ; 0Bh Set Color Palette
b_VIDFN0C      db      'Write Dot   ' ; 0Ch Write Dot
b_VIDFN0D      db      'Read Dot    ' ; 0Dh Read Dot
b_VIDFN0E      db      'Write TTY   ' ; 0Eh Write TTY
b_VIDFN0F      db      'Get State   ' ; 0Fh Return Current Video State
b_VIDFN10      db      'Set Pal Regs' ; 10h Set Palette Registers
b_VIDFN11      db      'Char Generat' ; 11h Character Generator Routine
b_VIDFN12      db      'Alter.Select' ; 12h Alternate Select
b_VIDFN13      db      'String Write' ; 13h Enhanced String Write
b_VIDFN14      db      'Load LCDFont' ; 14h Load LCD Character Font
b_VIDFN15      db      'Get Phys.Par' ; 15h Return Physical Display Parameters
b_VIDFN1A      db      'DisplCombCod' ; 1Ah Display Combination Code
b_VIDFN1B      db      'Func State  ' ; 1Bh Functionality/State Information
b_VIDFN1C      db      'SavRes.State' ; 1Ch Save/Restore Video State
;
; -----------------------------------------------------------------------
;   DOS functions related text descriptions
; -----------------------------------------------------------------------
;
b_DOSFN00 db 'Term.program' ; 00h    terminate program
b_DOSFN01 db 'Con Char Inp' ; 01h    get keyboard input
b_DOSFN02 db 'Con Char Out' ; 02h    display character to STDIO
b_DOSFN03 db 'Aux Char Inp' ; 03h    get character from STDAUX
b_DOSFN04 db 'Aux Char Out' ; 04h    output character to STDAUX
b_DOSFN05 db 'Prt Char Out' ; 05h    output character to STDPRN
b_DOSFN06 db 'CONIO echo  ' ; 06h    direct console I/O - keyboard to screen
b_DOSFN07 db 'CONIO noecho' ; 07h    get char from std I/O without echo
b_DOSFN08 db 'CONIO chkcbk' ; 08h    get char from std I/O without echo, checks for ^C
b_DOSFN09 db 'Display Str ' ; 09h    display a string to STDOUT
b_DOSFN0A db 'Buff.Kbd Inp' ; 0Ah    buffered keyboard input
b_DOSFN0B db 'Check Input ' ; 0Bh    check STDIN status
b_DOSFN0C db 'Clr Kbd Buff' ; 0Ch    clear keyboard buffer and invoke keyboard function
b_DOSFN0D db 'Clr Dsk Buff' ; 0Dh +  flush all disk buffers
b_DOSFN0E db 'Select Disk ' ; 0Eh +  select disk
b_DOSFN0F db 'Open FCB    ' ; 0Fh    open file with File Control Block
b_DOSFN10 db 'Close FCB   ' ; 10h    close file opened with File Control Block
b_DOSFN11 db 'FCB Find 1st' ; 11h    search for first matching file entry
b_DOSFN12 db 'FCB FindNext' ; 12h    search for next matching file entry
b_DOSFN13 db 'FCB Delete  ' ; 13h    delete file specified by File Control Block
b_DOSFN14 db 'FCB Seq.Read' ; 14h    sequential read from file specified by File Control Block
b_DOSFN15 db 'FCB Seq.Writ' ; 15h    sequential write to file specified by File Control Block
b_DOSFN16 db 'FCB Create  ' ; 16h    find or create firectory entry for file
b_DOSFN17 db 'FCB Rename  ' ; 17h    rename file specified by file control block

b_DOSFN19 db 'Get Cur.Disk' ; 19h +  return current disk drive
b_DOSFN1A db 'Set DTA Addr' ; 1Ah +  set disk transfer area (DTA)
b_DOSFN1B db 'Get Cur. FAT' ; 1Bh    get current disk drive FAT
b_DOSFN1C db 'Get FAT info' ; 1Ch    get disk FAT for any drive


b_DOSFN1F db 'GetDefDrvPar' ; 1Fh*   Get deafult drive parameter block

b_DOSFN21 db 'FCB Rnd Read' ; 21h    random read from file specified by FCB
b_DOSFN22 db 'FCB Rnd Writ' ; 22h    random write to file specified by FCB
b_DOSFN23 db 'Get FileSize' ; 23h    return number of records in file specified by FCB
b_DOSFN24 db 'SetRndRecSiz' ; 24h    set relative file record size field for file specified by FCB
b_DOSFN25 db 'Set Int Vect' ; 25h +  set interrupt vector
b_DOSFN26 db 'Create PSP  ' ; 26h    create new Program Segment Prefix (PSP)
b_DOSFN27 db 'FCB RndBRead' ; 27h    random file block read from file specified by FCB
b_DOSFN28 db 'FCB RndBWrit' ; 28h    random file block write to file specified by FCB
b_DOSFN29 db 'ParseCmdLine' ; 29h    parse the command line for file name
b_DOSFN2A db 'Get Sys Date' ; 2Ah +  get the system date
b_DOSFN2B db 'Set Sys Date' ; 2Bh +  set the system date
b_DOSFN2C db 'Get Sys Time' ; 2Ch +  get the system time
b_DOSFN2D db 'Set Sys Time' ; 2Dh +  set the system time
b_DOSFN2E db 'SetRes VERIF' ; 2Eh +  set/clear disk write VERIFY
b_DOSFN2F db 'Get DTA Addr' ; 2Fh +  get the Disk Transfer Address (DTA)
b_DOSFN30 db 'Get DOS ver.' ; 30h +  get DOS version number
b_DOSFN31 db 'Keep Process' ; 31h    TSR, files opened remain open
b_DOSFN32 db 'Read Ddsk PB' ; 32h*   read DOS Disk Block
b_DOSFN33 db 'SetResCtrlBk' ; 33h +  get or set Ctrl-Break
b_DOSFN34 db 'Get InDOS fl' ; 34h*   INDOS  Critical Section Flag
b_DOSFN35 db 'Get Int Vect' ; 35h +  get segment and offset address for an interrupt
b_DOSFN36 db 'Dsk Free spc' ; 36h +  get free disk space
b_DOSFN37 db 'Switch Char ' ; 37h*   get/set option marking character (SWITCHAR)
b_DOSFN38 db 'SetGet Intrn' ; 38h +  return country-dependent information
b_DOSFN39 db 'Cre SubDir  ' ; 39h +  create subdirectory
b_DOSFN3A db 'Del SubDir  ' ; 3Ah +  remove subdirectory
b_DOSFN3B db 'Change Dir  ' ; 3Bh +  change current directory
b_DOSFN3C db 'HND Create  ' ; 3Ch +  create and return file handle
b_DOSFN3D db 'HND Open    ' ; 3Dh +  open file and return file handle
b_DOSFN3E db 'HND Close   ' ; 3Eh +  close file referenced by file handle
b_DOSFN3F db 'HND Read    ' ; 3Fh +  read from file referenced by file handle
b_DOSFN40 db 'HND Write   ' ; 40h +  write to file referenced by file handle
b_DOSFN41 db 'HND Delete  ' ; 41h +  delete file
b_DOSFN42 db 'HND Move Ptr' ; 42h +  move file pointer (move read-write pointer for file)
b_DOSFN43 db 'GetSet Attr.' ; 43h +  set/return file attributes
b_DOSFN44 db 'Dev IOCTL   ' ; 44h +  device IOCTL (I/O control) info
b_DOSFN45 db 'HND Duplicat' ; 45h +  duplicate file handle
b_DOSFN46 db 'HND ForceDup' ; 46h +  force a duplicate file handle
b_DOSFN47 db 'Get CurrDir ' ; 47h +  get current directory
b_DOSFN48 db 'Alloc Memory' ; 48h +  allocate memory
b_DOSFN49 db 'Release Mem.' ; 49h +  release allocated memory
b_DOSFN4A db 'Modif. Mem. ' ; 4Ah +  modify allocated memory
b_DOSFN4B db 'Load/Exe Pgm' ; 4Bh +  load or execute a program
b_DOSFN4C db 'Quit Program' ; 4Ch +  terminate prog and return to DOS
b_DOSFN4D db 'SubProc RCod' ; 4Dh +  get return code of subprocess created by 4Bh
b_DOSFN4E db 'Find First  ' ; 4Eh +  find first matching file
b_DOSFN4F db 'Find Next   ' ; 4Fh +  find next matching file
b_DOSFN50 db 'Set new PSP ' ; 50h*   set new current Program Segment Prefix (PSP)
b_DOSFN51 db 'Get prog PSP' ; 51h*   puts current PSP into BX
b_DOSFN52 db 'Var LinkList' ; 52h*   pointer to the DOS list of lists
b_DOSFN53 db 'Conv.BPB-DPB' ; 53h*   translates BPB (Bios Parameter Block, see below)
b_DOSFN54 db 'Get VERIFY  ' ; 54h +  get disk verification status (VERIFY)
b_DOSFN55 db 'Create PSP 2' ; 55h*   create PSP: similar to function 26h
b_DOSFN56 db 'Rename File ' ; 56h +  rename a file
b_DOSFN57 db 'FileDateTime' ; 57h +  get/set file date and time
b_DOSFN58 db 'GetSet Alloc' ; 58h    get/set allocation strategy             (DOS 3.x)
b_DOSFN59 db 'Get ExtndErr' ; 59h +  get extended error information
b_DOSFN5A db 'Cre.TmpFile ' ; 5Ah +  create a unique filename
b_DOSFN5B db 'Cre.New File' ; 5Bh +  create a DOS file
b_DOSFN5C db 'Lock/UnlFile' ; 5Ch +  lock/unlock file contents
b_DOSFN5D db 'Internal Mul' ; 5Dh*   network
b_DOSFN5E db 'LAN Printer ' ; 5Eh*   network printer
b_DOSFN5F db 'LAN Redir   ' ; 5Fh*   network redirection
b_DOSFN60 db 'Pars.PathNam' ; 60h*   parse pathname

b_DOSFN62 db 'Get PSP 2   ' ; 62h    get program segment prefix (PSP)
b_DOSFN63 db 'Get LeadByte' ; 63h*   get lead byte table                     (DOS 2.25)

b_DOSFN65 db 'Get ExtCntry' ; 65h    get extended country information        (DOS 3.3)
b_DOSFN66 db 'SetRes CodeP' ; 66h    get/set global code page table          (DOS 3.3)
b_DOSFN67 db 'Set HND Cnt ' ; 67h    set handle count                        (DOS 3.3)
b_DOSFN68 db 'Commit File ' ; 68h    commit file                             (DOS 3.3)
b_DOSFN69 db 'Disk Serial ' ; 69h    disk serial number                      (DOS 4.0)


b_DOSFN6C db 'File Funcs. ' ; 6Ah    extended open/create combine file functions  (dos 4.0)
;
;
; -----------------------------------------------------------------------
;   MOUSE functions realted text descriptions
; -----------------------------------------------------------------------
;
b_MOUFN00      db      'Reset       ' ; 00h
b_MOUFN01      db      'Show Cursor ' ; 01h
b_MOUFN02      db      'Hide Cursor ' ; 02h
b_MOUFN03      db      'Get Position' ; 03h
b_MOUFN04      db      'Set Position' ; 04h
b_MOUFN05      db      'Get BPress  ' ; 05h
b_MOUFN06      db      'Get BRelease' ; 06h
b_MOUFN07      db      'Set H.Range ' ; 07h
b_MOUFN08      db      'Set V.Range ' ; 08h
b_MOUFN09      db      'Set Gr.Curs.' ; 09h
b_MOUFN0A      db      'Set Txt Curs' ; 0Ah
b_MOUFN0B      db      'Read Motion ' ; 0Bh
b_MOUFN0C      db      'Set Usr Hook' ; 0Ch
b_MOUFN0D      db      'Light Pen On' ; 0Dh
b_MOUFN0E      db      'Light Pen Of' ; 0Eh
b_MOUFN0F      db      'Mickey Ratio' ; 0Fh
b_MOUFN10      db      'Set Pal Regs' ; 10h
;
; ----------- END OF TRACKER CONTROL/DESCRIPTION TABLES -----------------
;
;
; =======================================================================
;   API ierface basic functions vectors table
; =======================================================================
;
fun_table    dw      f_dummy                   ;  0
             dw      f_link_ISR                ;  1
             dw      f_unlink_ISR              ;  2
             dw      f_uninstall               ;  3
             dw      f_link_ITV                ;  4
             dw      f_unlink_ITV              ;  5
             dw      f_dummy                   ;  6
             dw      f_dummy                   ;  7
;
; -----------------------------------------------------------------------
;   interrupt handler control blocks table
; -----------------------------------------------------------------------
;
BEGISRLIST     equ     $
ISR_list       label   byte
;
isr_08         db      008h
InISR08        db      0
prev_ISR08     dd      0
               dw      offset RESIDENT_GROUP:ISR08_int
;
isr_09         db      009h
InISR09        db      0
prev_ISR09     dd      0
               dw      offset RESIDENT_GROUP:ISR09_int
;
isr_2F         db      02fh
InISR2F        db      0
prev_ISR2F     dd      0
               dw      offset RESIDENT_GROUP:ISR2F_int
;
ENDISRLIST     equ     $
TSR_NINTH      equ     (ENDISRLIST-BEGISRLIST) / (size ISRCB)
;
RESIDENT_DATA  ends
;
; -----------------------------------------------------------------------
;   stack area
; -----------------------------------------------------------------------
;
RESIDENT_STACK segment word 'STACK'
               dw      TSR_STACKSIZE dup('@@')
top_of_stack   label   word
RESIDENT_STACK ends
;
;
; =======================================================================
;   TRANSIENT PART OF THE PROGRAM
; =======================================================================
;
TRANSIENT_GROUP group TRANSIENT_TEXT,TRANSIENT_DATA
;
TRANSIENT_TEXT segment para public 'TCODE'
               assume  cs:TRANSIENT_TEXT,ds:TRANSIENT_DATA,es:RESIDENT_DATA
               assume  ss:RESIDENT_STACK
;
;
; =======================================================================
;   WrAsciiz -- This sub writes an ASCIIZ string to the DOS console
; -----------------------------------------------------------------------
;   input:   ds:dx = ptr to the string to display
;
;   return:  none
; -----------------------------------------------------------------------
;   Destroy: dx,ax
; -----------------------------------------------------------------------
;   note: this routine assumes that DS will access TRANSIENT_DATA segment
; =======================================================================
;
WrAsciiz       proc    near
               .push   si
               mov     si,dx
@@nxt_ch:
               mov     dl,[si]             ; load char
               or      dl,dl               ; if char=0, end of string
                jz     @@end_msg
               mov     ah,02h              ; print char
               int     21h
               inc     si                  ; point to next char
                jmp    @@nxt_ch
@@end_msg:
               .pop    si
               ret
WrAsciiz       endp
;
;
; =======================================================================
;   cf_uninstall -- try to uninstall the resident instance of TRACKER
; -----------------------------------------------------------------------
;   input:   none
;
;   return:  none
; =======================================================================
;
cf_uninstall   proc    near
               cmp     [bInstOk],0            ; is TRACKER already resident?
                jz     @@cant_uninst          ; if not, give up end exit
               mov     ax,MPEXINTFUNC         ; use MPEX end of status call
               mov     bl,API_UNINSTALL       ; uninstall request
               int     2fh
               or      ah,ah                  ; any error?
                jc     @@cant_uninst          ; check for any error
               stc
               mov     al,RC_PGREMOVED        ; TSR program removed
               ret                            ; not an error, just terminate
@@cant_uninst:
               stc
               mov     al,RC_UNINSTERR        ; uninstall error
               ret                            ; back to caller
cf_uninstall   endp
;
;
; =======================================================================
;   cf_monovdout -- set output to monochormatic video
; -----------------------------------------------------------------------
;   input:   none
;
;   return:  none
; =======================================================================
;
cf_monovdout   proc    near
               clc                            ; signal no errors
               ret                            ; back to caller
cf_monovdout   endp
;
;
; =======================================================================
;   cf_resume -- resume tracing activity
; -----------------------------------------------------------------------
;   input:   none
;
;   return:  none
; =======================================================================
;
cf_resume      proc    near
               clc                            ; signal no errors
               ret                            ; back to caller
cf_resume      endp
;
;
; =======================================================================
;   cf_suspend -- suspend tracing activity
; -----------------------------------------------------------------------
;   input:   none
;
;   return:  none
; =======================================================================
;
cf_suspend     proc    near
               clc                            ; signal no errors
               ret                            ; back to caller
cf_suspend     endp
;
;
; =======================================================================
;   cf_help -- display help infos and terminate
; -----------------------------------------------------------------------
;   input:   none
;
;   return:  none
; =======================================================================
;
cf_help        proc    near
               stc                            ; use error cond. to terminate
               mov     al,RC_HELPINFOS        ; help request
               ret                            ; back to caller
cf_help        endp
;
;
; =======================================================================
;   initialization sequence -- entry point of TRACKER program
; =======================================================================
;
               assume  cs:TRANSIENT_TEXT,ds:RESIDENT_DATA,ss:RESIDENT_STACK
Install_TSR    proc    far
               mov     ax,RESIDENT_DATA  ; access resident PART of data
               mov     ds,ax
               mov     [wPSPSeg],es      ; save PSP segment
;
; -----------------------------------------------------------------------
;   check MS-DOS version, must be 3.0 or greater
; -----------------------------------------------------------------------
;
check_DOSVER   label   near
               mov     ah,30h
               int     21h
               cmp     al,3
                jae    @@vers_ok
               mov     al,RC_INVDOSVER
               jmp     @@error_exit
;
; -----------------------------------------------------------------------
;   verify that this TSR is not already loaded
; -----------------------------------------------------------------------
;
@@vers_ok:
               mov     [wVersion],ax       ; keep major and minor version number
               mov     bx,es:[PSPENV]      ; bx = Environment Segment address
               mov     [wENVSeg],bx        ; keep it
               mov     ah,MULTIPLEXID      ; ah = our multiplex ID number
               xor     al,al               ; get installed state
               int     2fh
               mov     es,[wPSPSeg]        ; es = PSP segment, ds = data segment
               assume  ds:TRANSIENT_DATA,es:NOTHING
               mov     dx,TRANSIENT_DATA
               mov     ds,dx               ; ds = transient data segment
               mov     [bInstOk],al        ; if al = 0 ok to install
               .print_string szM_STARTUP   ; display start up message
;
; -----------------------------------------------------------------------
;   check if a line switch /U if present (uninstall command)
; -----------------------------------------------------------------------
;
@@check_parms:
               mov     di,PSPCMD           ; di = command tail offset into PSP
               xor     cx,cx
               or      cl,es:[di]          ; check length of command line
                jz     @@no_parms          ; no parameters at all
               inc     di                  ; skip length
@@parse_next:
               mov     al,'/'              ; al = switch prefix char
               repne   scasb               ; scan for cmd line switches
                jne    @@no_parms          ; no more parms to parse
               mov     al,es:[di]          ; get next character
               cmp     al,'A'              ; check if below alpha range
                jb     @@no_lower
               cmp     al,'Z'              ; check if above alpha range
                ja     @@no_lower
               or      al,20h              ; make it lowercase
@@no_lower:
               .push   es,cx,di
               push    TRANSIENT_DATA
               pop     es                  ; es = transient data segment
               assume  es:TRANSIENT_DATA
               mov     di,offset TRANSIENT_DATA:bCmdSwitches
               mov     cx,CMDLINESWLEN
               repne   scasb               ; scan for valid commands
               mov     bx,cx               ; copy dispatch index into slot ptr
               .pop    es,cx,di            ; restore cmd line parse regs
               mov     al,RC_INVLINCMD     ; invalid command line parm
                jne    @@parse_error       ; abort parsing
               shl     bx,1                ; convert dispatch index to word ofs
               .push   es,cx,di            ; save used regs
               call    [bx+wCmdLineTable]  ; call command routine
               .pop    es,cx,di            ; restore cmd line parse regs
                jc     @@parse_error       ; abort parsing if any error
                jcxz   @@no_parms          ; no parameters are present
                jmp    short @@parse_next  ; continue parsing
@@parse_error:
                jmp    @@error_exit        ; manage the error
@@no_parms:
                jmp    @@get_DOSflags      ; no parameters present
;
; -----------------------------------------------------------------------
;   get various informations about the current MS-DOS
; -----------------------------------------------------------------------
;
@@get_DOSflags:
               assume  es:NOTHING
               cmp     [bInstOk],0        ; if  = 0 ok to install
                jz     @@ok_to_inst
               mov     al,RC_PGYETINST    ; program is already installed
                jmp    @@error_exit
@@ok_to_inst:
               mov     ax,RESIDENT_DATA
               mov     ds,ax
               assume  ds:RESIDENT_DATA
               mov     ah,34h             ; return pointer of InDOS flag
               int     21h                ; es:bx -> InDOS flag
               mov     [wInDOS_ofs],bx    ; save the pointer
               mov     [wInDOS_seg],es
               mov     ax,[wVersion]
               cmp     ax,030ah           ; check version
                jb     pre_dos310
               cmp     ax,0a00h           ; is a MS OS/2-DOS 3.0 box ?
                jae    pre_dos310
               dec     bx
               mov     [wErrMod_seg],bx   ; assume ErrorMode is in the same
                jmp    short shrink_mem   ; segment as InDOS
pre_dos310:
               mov     cx,0ffffh          ; scan MS-DOS segment for ErrorMode
               xor     di,di
search_loop1:
               mov     ax,word ptr cs:[LF2]    ; ax = opcode for int 28h
search_loop2:
               repne   scasb
                jne    intopc_nfound
               cmp     ah,es:[di]              ; inspect 2nd byte of opcode
                jne    search_loop2            ; serach again ...
               mov     ax,word ptr cs:[LF1+1]  ; ax = opcode cof CMP
               cmp     ax,es:[di][LF1-LF2]     ; check opcode
                jne    opc_not_cmp
               mov     ax,es:[di][(LF1-LF2)+2] ; ax = offset Error Mode
                jmp    short sav_ErrModptr     ; in MS-DOS segment
opc_not_cmp:
               mov     ax,word ptr cs:[LF3+1]  ; ax = opcode for TEST
               cmp     ax,es:[di][LF3-LF4]     ; check for TEST
                jne    search_loop1            ; if not TEST continue the search
sav_ErrModptr:
               mov     [wErrMod_ofs],ax
                jmp    short shrink_mem        ; go ahead
intopc_nfound:
               mov     al,RC_EMNOFOUND
                jmp    @@error_exit
;
; =======================================================================
;   code fragments used for scanning for ErrorMode flag
; -----------------------------------------------------------------------
;   note: this code needs only to be read, it is not executed !
; -----------------------------------------------------------------------
               assume  ss:NOTHING
LFnear         label   near
LFbyte         label   byte
LFword         label   word
LF1:           cmp     ss:LFbyte,0       ; MS-DOS versions earlier than 3.1
                jne    LFnear            ; check ErrorMode, 0
LF2:           int     28h
LF3:           test    ss:LFbyte,0ffh    ; MS-DOS versions 3.1 and later
                jne    LFnear            ; check ErrorMode, 0ffh
               push    ss:LFword
LF4:           int     28h
               assume  ss:RESIDENT_STACK
; =======================================================================
;
; -----------------------------------------------------------------------
;   reduce the memory required by this program
; -----------------------------------------------------------------------
;
shrink_mem     label   near
               mov     es,[wPSPSeg]            ; es = PSP segment
               mov     bx,TRANSIENT_STK
               add     bx,TRANSTKPARAGS
               sub     bx,RESIDENT_TEXT
               add     bx,11h                  ; add psp size and round it
               mov     ah,4ah                  ; set memory block function
               int     21h                     ; call MS-DOS
                jnc    @@link_ISR              ; ok, continue
               mov     al,RC_MALLOCERR         ; memory allocation error
                jmp    @@error_exit
;
; -----------------------------------------------------------------------
;   link ISR vectors
; -----------------------------------------------------------------------
;
@@link_ISR     label   near
               mov     ax,MPEXINTFUNC      ; use MPEX end of status call
               mov     bl,API_LINKISR      ; link ISRs request
               pushf                       ; simulate INT instr.
               call    ISR2F_int
;
; -----------------------------------------------------------------------
;   link ITV vectors
; -----------------------------------------------------------------------
;
@@link_ITV     label   near
               mov     ax,MPEXINTFUNC      ; use MPEX end of status call
               mov     bl,API_LINKITV      ; link ITVs request
               int     2fh                 ; ignore error code
;
; -----------------------------------------------------------------------
;    Set up the info screen (temporary)
; -----------------------------------------------------------------------
;
               mov     ax,RESIDENT_GROUP
               mov     es,[wVSeg]
               assume  ds:TRANSIENT_DATA
               mov     ax,TRANSIENT_DATA
               mov     ds,ax
               mov     si,offset TRANSIENT_DATA:bHdrLine1
               mov     cx,80
               mov     ah,70h
               xor     di,di
@@disp_header:
               lodsb
               stosw
                loop   @@disp_header
               mov     cx,80
               mov     ah,0fh
@@disp_uppfrm:
               lodsb
               stosw
                loop   @@disp_uppfrm
               mov     bx,20
@@disp_nexfrm:
               mov     si,offset TRANSIENT_DATA:bHdrLine3
               mov     cx,80
@@disp_rowfrm:
               lodsb
               stosw
                loop   @@disp_rowfrm
               dec     bx
                jnz    @@disp_nexfrm
               mov     si,offset TRANSIENT_DATA:bHdrLine4
               mov     cx,80
@@disp_lowfrm:
               lodsb
               stosw
                loop   @@disp_lowfrm
;
; -----------------------------------------------------------------------
;   make resident the the loaded modules
; -----------------------------------------------------------------------
;
make_resident  label   near
               mov     ax,RESIDENT_DATA
               mov     ds,ax
               assume  ds:RESIDENT_DATA
               mov     ax,[wPSPSeg] ; ax = PSP segment
               mov     dx,cs        ; dx = paragraph address of start of
               sub     dx,ax        ;      transient portion
               mov     ax,3100h     ; terminate and stay resident
               int     21h
;
; -----------------------------------------------------------------------
;   common errors/no resident exit point
; -----------------------------------------------------------------------
;
@@error_exit   label   near
               push    ax                     ; save return code
               xor     ah,ah                  ; clear high byte
               mov     dx,TRANSIENT_DATA      ; access data segment
               mov     ds,dx
               shl     ax,1                   ; convert to word displacement
               mov     bx,offset TRANSIENT_DATA:wMsgPtrs
               add     bx,ax
               mov     dx,[bx]                ; get message ptr
               call    WrAsciiz               ; writes an ASCII string
               pop     ax                     ; restore return code
               mov     ah,4ch                 ; terminate process function
               int     21h
Install_TSR    endp
;
TRANSIENT_TEXT ends
;
; -----------------------------------------------------------------------
;   transient data
; -----------------------------------------------------------------------
;
TRANSIENT_DATA segment word public 'TDATA'
TRANSDATASTART equ     $
;
; -----------------------------------------------------------------------
;   messages/error codes indexes
; -----------------------------------------------------------------------
;
RC_MALLOCERR   equ     0               ; memory allocation error
RC_INVLINCMD   equ     1               ; invalid command line switch
RC_CMDFUNERR   equ     2               ; error from command handler routine
RC_INVDOSVER   equ     3               ; invalid dos version
RC_UNINSTERR   equ     4               ; TSR uninstall error
RC_PGREMOVED   equ     5               ; program has been removed
RC_PGYETINST   equ     6               ; program is already installed
RC_EMNOFOUND   equ     7               ; DOS error mode flag not found
RC_HELPINFOS   equ     8               ; help message
;
; -----------------------------------------------------------------------
;   messages table pointers
; -----------------------------------------------------------------------
;
wMsgPtrs       label   word
               dw      TRANSIENT_DATA:szM_MALLOCERR
               dw      TRANSIENT_DATA:szM_INVLINCMD
               dw      TRANSIENT_DATA:szM_CMDFUNERR
               dw      TRANSIENT_DATA:szM_INVDOSVER
               dw      TRANSIENT_DATA:szM_UNINSTERR
               dw      TRANSIENT_DATA:szM_PGREMOVED
               dw      TRANSIENT_DATA:szM_PGYETINST
               dw      TRANSIENT_DATA:szM_EMNOFOUND
               dw      TRANSIENT_DATA:szM_HELPINFOS
;
; -----------------------------------------------------------------------
;   messages strings
; -----------------------------------------------------------------------
;
szM_STARTUP    db      'TRACKER v0.1 - (C) ARTIS FFiorese, 1992',13,10
               db      'Interrupt Services Interceptor',13,10,0
szM_MALLOCERR  db      'memory allocation error',13,10,0
szM_INVLINCMD  db      'unsupported command line switch',13,10,0
szM_CMDFUNERR  db      'execution error',13,10,0
szM_INVDOSVER  db      'invalid DOS version (must be 3.0 or greater)',13,10,7,0
szM_UNINSTERR  db      'program is not yet resident',,13,10,7,0
szM_PGREMOVED  db      'resident copy has been sucessfully removed',13,10,0
szM_PGYETINST  db      'program already installed',13,10,0
szM_EMNOFOUND  db      'error: error DOS mode flag pointer not found',13,10,0
szM_HELPINFOS  db      'usage: TRACKER [/U] [/R] [/S] [/M] [/?] [/H]',13,10,10
               db      9,'/U = uninstall the resident copy',13,10
               db      9,'/R = resume monitoring',13,10
               db      9,'/S = suspend monitoring',13,10
               db      9,'/M = use mono display adapter at startup',13,10
               db      9,'/?,/H = display this help',13,10,0
;
; -----------------------------------------------------------------------
;   command line switches array
; -----------------------------------------------------------------------
;
CMDLINESWBEG   =       $
bCmdSwitches   label   byte
               db      'm'               ; use mono video
               db      'r'               ; resume
               db      's'               ; suspend
               db      'u'               ; uninstall
               db      '?'               ; help request
               db      'h'               ; help request
CMDLINESWLEN   =       $ - CMDLINESWBEG
;
; -----------------------------------------------------------------------
;   command line switches handler table pointers
;   note: is in reverse order in respect to the cmd line switches array
; -----------------------------------------------------------------------
;
wCmdLineTable  label   word
               dw      cf_help
               dw      cf_help
               dw      cf_uninstall
               dw      cf_suspend
               dw      cf_resume
               dw      cf_monovdout
;
bInstOk        db      0                 ; installed flag
;
; -----------------------------------------------------------------------
;   Info screen layout
; -----------------------------------------------------------------------
;
bScrHeader     label   byte
bHdrLine1      db      'No ICat   Service     CS   IP   DS   ES   AX   BX  '
               db      ' CX   DX   SI   DI   BP   FL '
bHdrLine2      db      '���������������������������������������������������'
               db      '�����������������������������'
bHdrLine3      db      '  �    �            �    �    �    �    �    �    �'
               db      '    �    �    �    �    �    '
bHdrLine4      db      '���������������������������������������������������'
               db      '�����������������������������'
;
;
TRANSDATALEN   equ     (($ - TRANSDATASTART) / 16)
TRANSIENT_DATA ends
;
; -----------------------------------------------------------------------
;   transient stack area
; -----------------------------------------------------------------------
;
TRANSTKPARAGS = TRANSTACKSIZE/8
;
TRANSIENT_STK  segment word stack 'TSTACK'
               dw      TRANSTACKSIZE dup('FF')
TRANSIENT_STK  ends
               end     Install_TSR

; =======================================================================
;                               IPLKEY
;
;                  by Franco Fiorese (C)  1989, 1990
;
; =======================================================================
;   module:          iplkey
;   description:     ipl controlled by key access
;   created by:      Franco Fiorese
;   date:            14-Apr-90
;   last mod. date:  14-Apr-90
;   current version: 1.00
; =======================================================================
;   revision history:
;
;     date   author  description
;   --------- --- -------------------------------------------------------
;
;
; =======================================================================
;
;
;
; =======================================================================
;   print_string -- write an ASCII$ string to DOS console
; =======================================================================
;
print_string   macro   @string
               lea     dx,@string
               mov     ah,09               ;; write an ascii string
               int     21h
               endm
;
; -----------------------------------------------------------------------
;
               DOSSEG
               .MODEL  small
               .STACK  100h

               .DATA

secbuffer      db      512 dup(0)          ; buffer used for read/write
imgname        db      'bootimg.dat',0     ; boot image file name
option         db      0                   ; selected option
;
; -----------------------------------------------------------------------
;   messages
; -----------------------------------------------------------------------
;
start_up_msg   db      'IPLKEY v1.0 - IPL key controlled access',13,10
               db      'by Franco Fiorese (C) 1990',13,10,'$'
;
help_msg       db      13,10,' usage: iplkey { /S | /R | /Ppassword }',13,10
               db      ' /S = backup boot record to the image file',13,10
               db      ' /R = restore boot record from the image file',13,10
               db      ' /P = password max 8 chars ',13,10
               db      'one of the three options /S /P or /R must be'
               db      ' present !',7,13,10,'$'
;
;
; -----------------------------------------------------------------------
;   error messages
; -----------------------------------------------------------------------
;
err_01_msg     db      'error: cannot read boot sector !',7,0d,0ah,'$'
err_02_msg     db      'error: cannot write boot sector !',7,0d,0ah,'$'
err_03_msg     db      'error: cannot create image file !',7,0d,0ah,'$'
err_04_msg     db      'error: cannot read image file !',7,0d,0ah,'$'
err_05_msg     db      'error: image file BOOTIMG.DAT is read only, '
               db      'cannot create a new one !',7,13,10,'$'
err_06_msg     db      'error: invalid password length (3..8 characters) !'
               db      7,0d,0ah,'$'
;
               .CODE
;
; =======================================================================
;      code to put the iplkey key object code into the boot record
; =======================================================================
;
;
put_iplkey     proc    near
               mov     ax,@DATA
               mov     ds,ax
               print_string start_up_msg
;
; -----------------------------------------------------------------------
;   1 -- parse command line for "/R" or "/P" options
; -----------------------------------------------------------------------
;
               mov     ax,es               ; save PSP address
               mov     ds,ax               ; ds = es = PSP area

               mov     si,80h              ; command tail
               lodsb                       ; get the length
               or      al,al               ; check if none
               jz      init6
               mov     cl,al
               sub     ch,ch               ; put count in cx
init4:
               lodsb                       ; load next byte
               cmp     al,'/'              ; check if switch
               je      init7               ; jump if so
init5:
               loop    init4               ; otherwise loop back
init6:
               mov     ax,@DATA
               mov     ds,ax
               print_string help_msg       ; no recognized switches
               jmp     end_pgm             ; terminate execution
;
init7:
               dec     cx                  ; reduce count
               jz      init6               ; jump if no more bytes
               lodsb                       ; load command character
               cmp     al,'Z'
               jbe     no_upper
               sub     al,'a'-'A'          ; convert to upper-case
no_upper:
               cmp     al,'R'              ; check if Restore
               jne     init8               ; if not, check other switch
               jmp     rebuild_boot        ; rebuild the boot sector
init8:
               cmp     al,'S'              ; check if Backup
               jne     init9               ; if not, check other switch
               jmp     get_boot_sec        ; go to save boot sector
init9:
               cmp     al,'P'              ; check if Password
               jne     init5               ; if not, go back to loop
               mov     ax,@CODE
               mov     es,ax
               mov     di,offset @CODE:pass_key
               mov     cx,8                ; cx = password max length
               cmp     byte ptr [si+1],' '
               je      inv_passwd
copy_passwd:
               lodsb
               cmp     al,0dh              ; check for cmd line end
               je      store_pw_len
               xor     al,cl               ; do a stupid password encryption
               stosb
               loop    copy_passwd
store_pw_len:
               mov     ax,8
               sub     ax,cx
               mov     cs:[pass_len],al    ; save new password length
               mov     al,'P'
inv_passwd:
               mov     dx,@DATA
               mov     ds,dx
               mov     es,dx
               cmp     cs:[pass_len],3     ; check password length
               jae     passwd_ok
               print_string err_06_msg
               jmp     bad_term
passwd_ok:
               jmp     get_boot_sec
;
; -----------------------------------------------------------------------
;   2 -- rebuild boot sector using the image file
; -----------------------------------------------------------------------
;
rebuild_boot:
               mov     ax,@DATA
               mov     ds,ax
               mov     es,ax
               mov     ax,3d00h                ; DOS create a file
               mov     dx,offset @DATA:imgname ; get file name
               int     21h                     ; execute
               jnc     open_ok
read_img_err:
               print_string err_04_msg
               jmp     bad_term
open_ok:
               mov     bx,ax                   ; get file handle
               push    bx                      ; save it
               mov     cx,512                  ; 1 sector size
               mov     dx,offset secbuffer     ; get file name
               mov     ah,3fh                  ; DOS read handle
               int     21h                     ; execute
               pop     bx                      ; restore file handle
               jc      read_img_err
               mov     ah,3eh                  ; DOS close file
               int     21h                     ; execute
               jc      read_img_err
               jmp     write_bootsec
;
; -----------------------------------------------------------------------
;   3 -- get partition table sector data
; -----------------------------------------------------------------------
;
get_boot_sec:
               mov     dx,@DATA
               mov     ds,dx
               mov     es,dx
               mov     [option],al         ; save option
               mov     ax,0201h            ; read 1 sector
               mov     dx,0080h            ; head 0, drive 80h (1st hard disk)
               mov     bx,offset secbuffer ; buffer used for read/write
               mov     cx,0001h            ; start with sector 1
               int     13h
               jnc     check_option
create_img_err:
               print_string err_01_msg
               jmp     bad_term

check_option:
               cmp     [option],'P'        ; Password option ?
               je      copy_iplkey
;
; -----------------------------------------------------------------------
;   4 -- makes an image file of the partition table sector
; -----------------------------------------------------------------------
;
read_ok:
               mov     ah,3ch                  ; DOS create a file
               mov     dx,offset @DATA:imgname ; get file name
               mov     cx,01h                  ; read only attributes
               int     21h                     ; execute
               jnc     create_ok
               cmp     ax,5
               jnz     creat_err
               print_string err_05_msg
               jmp     bad_term
creat_err:
               print_string err_03_msg
               jmp     bad_term
create_ok:
               mov     bx,ax                   ; get file handle
               push    bx                      ; save it
               mov     cx,512                  ; 1 sector size
               mov     dx,offset secbuffer     ; get file name
               mov     ah,40h                  ; DOS write to handle
               int     21h                     ; execute
               pop     bx                      ; restore file handle
               jc      create_img_err
               mov     ah,3eh                  ; DOS close file
               int     21h                     ; execute
               jc      create_img_err
               cmp     [option],'S'
               jne     copy_iplkey
               jmp     end_pgm
;
; -----------------------------------------------------------------------
;   5 -- put the iplkey code into sector buffer
; -----------------------------------------------------------------------
;
copy_iplkey:
               mov     si,offset @CODE:iplkey ; get start of iplkey code
               mov     di,offset secbuffer    ; disk buffer offset
               mov     byte ptr es:[di+25],0f0h ; set jump to the iplkey code
               add     di,0f0h                ; displacement where to load to
               mov     cx,IPLKEYCODLEN        ; iplkey code length
               cld
               push    ds
               mov     ax,cs
               mov     ds,ax
               rep     movsb               ; store it into boot record buffer
               pop     ds
;
; -----------------------------------------------------------------------
;   6 -- writes out the modified sector buffer
; -----------------------------------------------------------------------
;
write_bootsec:
               mov     ax,0301h            ; write 1 sector
               mov     dx,0080h            ; head 0, drive 80h (1st hard disk)
               mov     bx,offset secbuffer ; buffer used for read/write
               mov     cx,0001h            ; start with sector 1
               int     13h
               jnc     end_pgm
               print_string err_02_msg
               jmp     bad_term
;
; -----------------------------------------------------------------------
;   7 -- good termination
; -----------------------------------------------------------------------
;
end_pgm:
               mov     ax,4c00h            ; DOS terminate process function
               int     21h
;
; -----------------------------------------------------------------------
;   ERR -- disk read or write error
; -----------------------------------------------------------------------
;
bad_term:
               mov     ah,0                ; reset disk system
               int     13h
               mov     ax,4cffh            ; DOS terminate process function
               int     21h
put_iplkey     endp

;
; =======================================================================
;   iplkey -- code used for controlled ipl access
; -----------------------------------------------------------------------
;   input:   none
;
;   return:  none
; =======================================================================
;
               assume  SS:@CODE,DS:@CODE,ES:@CODE
               org     61dh
ipl_code       label   near
               org     6f0h
iplkey         proc    near

;
; -----------------------------------------------------------------------
;   display the "key" asking for the password
; -----------------------------------------------------------------------
;
               mov     ax,0700h           ; clear screen
               mov     bx,0700h
               xor     cx,cx
               mov     dx,194fh
               int     10h
;
               call    disp_key
;
; -----------------------------------------------------------------------
;   get the password
; -----------------------------------------------------------------------
;
               mov     bx,3               ; max 3 attempts permitted
get_key:
               mov     cx,8               ; input buffer length
               mov     di,offset @CODE:key_buff ; es:di = input buffer
               push    di
               xor     al,al              ;
               rep     stosb              ; clear input buffer
               pop     di
               mov     cx,8               ; max inp. buffer is 8 characters
read_key:
               xor     ax,ax              ; ah = BIOS func read keyboard
               int     16h
               cmp     al,0dh             ; check for Enter being pressed
               je      end_read           ; if so  terminate the input ...
               xor     al,cl              ; do a stupid password decryption
               stosb                      ; otherwise continue until ...
               loop    read_key           ; ... max length is reached
;
; -----------------------------------------------------------------------
;   check the password
; -----------------------------------------------------------------------
;
end_read:
               mov     si,offset @CODE:key_buff ; input buffer offset
               mov     di,offset @CODE:pass_key ; keyword buffer offset
               mov     cl,[pass_len]      ; get password length
chk_key:
               rep     cmpsb              ; compare strings
               jnz     short next_attemp  ; if not ok allow another chance ?
               mov     ah,2
               xor     bx,bx
               xor     dx,dx              ; put cursor at home position
               int     10h
               jmp     ipl_code           ; password ok, boot permitted
next_attemp:
               dec     bx                 ; decrement attempts counter
               jnz     get_key            ; another chance ?
               mov     [key_str2],7bh     ; load 'X' symbol to signal ...
               call    disp_key           ; ...that all attempts are failed ...
               jmp     short $            ; ...then hang up the system
;
;
; =======================================================================
;   displays a little key on the screen
; -----------------------------------------------------------------------
;   input:   none
;
;   return:  none
; =======================================================================
;
disp_key       label   near
               mov     ah,2
               xor     bx,bx
               xor     dx,dx              ; put cursor at home position
               int     10h
               mov     si,offset @CODE:key_str
               mov     cx,5
disp_loop:
               push    cx
               lodsb
               xor     al,cl              ; decrypt key symbol char
               mov     ah,0eh
               mov     bx,0007h
               xor     dx,dx
               mov     cx,1
               int 10h                    ; display string characters
               pop     cx
               loop    disp_loop
;
               mov     ah,2
               xor     bx,bx
               mov     dx,1900h           ; make cursor invisible
               int     10h
               ret
;
; -----------------------------------------------------------------------
;   iplkey local data (resides in the iplkey code segment)
; -----------------------------------------------------------------------
;
pass_len       db      6
pass_key       db      8 dup(0)
key_buff       db      8 dup(0)
key_str        db      0d3h,0c0h
key_str2       db      0c7h,0c6h,06eh

IPLKEYCODLEN   equ     $ - iplkey
;
iplkey         endp
               end

;* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
;*                                                                       *
;*  nome modulo  : VCLOCK.ASM                                            *
;*  scritto da   : Franco Fiorese                                        *
;*  versione     : 2.02                                                  *
;*  data creaz.  : Jan-85                                                *
;*  descrizione  : desk clock programmabile.                             *
;*                                                                       *
;*  Revision history:                                                    *
;*                                                                       *
;*  data       eseguita da               descrizione                     *
;*  ---------  -------------------  ---------------------------------    *
;*  16-Mar-87  Fiorese Franco       revisione completa (rel. 2.00)       *
;*  16-Mar-87  Fiorese Franco       impl. allarme                        *
;*  17-Mar-87  Fiorese Franco       eliminaz. flicker su video colore    *
;*  22-Apr-87  Fiorese Franco       variaz. su shift keys                *
;*  22-Apr-87  Fiorese Franco       impl. stop allarme con shift keys    *
;*  25-Jan-96  Fiorese Franco       removev CGA snow retrace control     *
;*                                                                       *
;* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
;
MMTicks  equ       1092                ; ticks in one minute (18.2 * 60)
freq     equ       1000h               ; counter value for sound freq.
NUMCOL   equ       0eh
CHCHRCOL equ       0ch
CHSEPCOL equ       09h
;
code     segment   para public 'code'
         assume    cs:code,ds:code,es:code
;
         org       100h
;
vclock   proc      far
start:   jmp       init
         jmp       prog
magic:
         dw        1234h
;
; -----------------------------------------------------------------------
;    flags:
;
;    bit 0      0000000x = 1  reentrance flag
;    bit 1      000000x0 = 1  alarm enabled
;    bit 2      00000x00 = 1  alarm is ringing
;    bit 3      0000x000 = 1  color video adapter active
;    bit 4      000x0000 = 1  display time on
;    bit 5      00x00000 = *  not used
;    bit 6      0x000000 = *  not used
;    bit 7      x0000000 = 1  perform IPL when timer expires
;
; -----------------------------------------------------------------------
;
VidSeg   dw        0b800h              ; video seg address
flKey    db        0
         db        'the flags->'
flags    db        000100000b          ; various flags (see above)
CountSC  dw        0                   ; ticks counter for seconds display
CountTK  db        0                   ; ticks   counter
CountSS  db        0                   ; seconds counter
CountMM  db        0                   ; minutes counter
CountHH  db        0                   ; hours   counter
AlarmMM  db        0                   ; alarm minutes
AlarmHH  db        0                   ; alarm hour
wBeepCnt dw        0                   ; beep count
TimeCol  db        0Eh                 ; digits color
decbuf   db        4 dup('0')          ; conversion buffer
lowmes   db        'Low '
uppmes   db        'Upp '
curmes   db        'Cur '
nummes   db        'Num '
;
prog:    inc       cs:CountTK
         inc       cs:CountSC
         test      cs:flags,00000001b  ; check for reentrance
         jz        work
         jmp       nowork
;
; -----------------------------------------------------------------------
;
; -----------------------------------------------------------------------
;
work:    or        cs:flags,00000001b  ; working signal
         push      di
         push      dx
         push      ds
         push      es
         push      bp
         push      si
         push      cx
         push      bx
         push      ax
         pushf
         mov       ax,cs
         mov       ds,ax
         mov       di,140
         mov       ax,cs:VidSeg
         mov       es,ax
         cmp       cs:CountSC,MMTicks
         jb        noresSC
         mov       cs:CountSC,0
noresSC:
         cmp       cs:CountTK,91       ; 91 ticks = 5 seconds
         jnz       wait1
         mov       cs:CountTK,0
         add       cs:CountSS,5        ; inc seconds counter
         cmp       cs:CountSS,60
         jb        wait1
         mov       cs:CountSS,0
         inc       cs:CountMM
         cmp       cs:CountMM,60
         jnz       chkbell
         mov       cs:CountMM,0
         inc       cs:CountHH
         cmp       cs:CountHH,24
         jnz       chkbell
         mov       cs:CountHH,0
;
;  test for sound alarm
;
chkbell:
         test      cs:flags,00000100b  ; bell is on ?
         jnz       BellAct
         test      cs:flags,00000010b  ; alarm is enabled ?
         jz        wait1
         mov       al,cs:CountHH
         cmp       cs:AlarmHH,al
         jne       wait1
         mov       al,cs:CountMM
         cmp       cs:AlarmMM,al
         jne       wait1
BellOk:  call      Bell_On             ; turn alarm on
         or        cs:flags,00000100b  ; bell on signal
         jmp       short wait1
BellAct: call      Bell_Off            ; turn alarm off
         and       cs:flags,11111011b  ; alarm off signal
;
wait1:
         test      cs:flags,00000100b  ; bell is on ?
         jnz       okdisp              ; yes display time in blinking
;
; -----------------------------------------------------------------------
;   check for shift states
; -----------------------------------------------------------------------
;
         call      GetKStat            ; get DOS key shift status
         mov       cs:flKey,al         ; store al in memory
;
; -----------------------------------------------------------------------
;   check for Ctrl + Shift (time display on)
; -----------------------------------------------------------------------
;
         test      al,00000100b        ; test Ctrl key
          jz       no_dispon
         test      al,00000010b        ; test left shift key
          jz       no_dispon
         or        cs:flags,00010000b  ; set display flag
          jmp      short no_dispoff
;
; -----------------------------------------------------------------------
;   check for Alt + Shift (time display off)
; -----------------------------------------------------------------------
;
no_dispon:
         test      al,00001000b        ; test Alt key
          jz       no_dispoff
         test      al,00000010b        ; test left shift key
          jz       no_dispoff
         and       cs:flags,11101111b  ; reset display flag
;
no_dispoff:
         test      cs:flags,00010000b  ; check for display flag
         jnz       okdisp
         jmp       nodisp
okdisp:
         mov       al,cs:CountHH
         call      bindec
         push      cx
         mov       si,offset decbuf + 1
         mov       ah,TimeCol
         mov       cx,2
disphou: lodsb
;         call      vstosw
         stosw
         loop      disphou
         mov       al,':'
         mov       ah,CHSEPCOL
;         call      vstosw
         stosw
         pop       cx
;
         mov       al,cs:CountMM
         call      bindec
         push      cx
         mov       si,offset decbuf + 1
         mov       ah,TimeCol
         mov       cx,2
dispmin:
         lodsb
;         call      vstosw
         stosw
         loop      dispmin
         mov       al,':'
         mov       ah,CHSEPCOL
;         call      vstosw
         stosw
         pop       cx
;
         mov       ax,cs:CountSC
         mov       cx,10
         mul       cx
         xor       dx,dx
         mov       cx,182
         div       cx
         test      cs:flags,00000100b  ; alarm active ?
         jz        noactiv
         push      ax
         call      GetKStat
         test      al,00000001b        ; test left shift key
         jz        no_off
         test      al,00000010b        ; test right shift key
         jz        no_off
         and       cs:flags,11111001b  ; disable alarm
         mov       cs:[wBeepCnt], 0    ; reset ringing time counter
         pop       ax
         and       al,11111110b
         push      ax
no_off:
         pop       ax
         test      al,1
         jz        skip1
         push      ax
         call      Bell_On
         pop       ax
         jmp       short noactiv
skip1:   push      ax
         call      Bell_Off
         pop       ax
noactiv: call      bindec
         push      cx
         mov       si,offset decbuf + 1
         mov       ah,TimeCol
         mov       cx,2
dispsec:
         lodsb
;         call      vstosw
         stosw
         loop      dispsec
         pop       cx
;
         mov       cx,4
         mov       di,124
         call      GetKstat
         test      al,01000000b
         mov       ah,CHCHRCOL
         jz        lowerc
         mov       si,offset uppmes
         jmp       putinf1
lowerc:
         mov       si,offset lowmes
putinf1:
         lodsb
;         call      vstosw
         stosw
         loop      putinf1
;
         mov       cx,4
         mov       di,132
         call      GetKstat
         test      al,00100000b
         mov       ah,CHCHRCOL
         jz        curset
         mov       si,offset nummes
         jmp       putinf2
curset:
         mov       si,offset curmes
putinf2:
         lodsb
;         call      vstosw
         stosw
         loop      putinf2
;
         mov       di,122
         mov       al, '*'             ; simple alarm indicator
         test      cs:flags,10000000b  ; check for IPL enabled
          jz       no_ipl
         mov       al, '!'             ; alarm with auto-reboot (IPL) symbol
no_ipl:
         test      cs:flags,00000010b  ; check for alarm enabled
          jz       alarm_off
         mov       ah, 8ch
          jmp      disp_ax
alarm_off:
         mov       ah, 00h
disp_ax:
         stosw
nodisp:
         popf
         pop       ax
         pop       bx
         pop       cx
         pop       si
         pop       bp
         pop       es
         pop       ds
         pop       dx
         pop       di
         and       cs:flags,11111110b  ; reset reentr. flag
nowork:  sti
         iret
;
; -----------------------------------------------------------------------
; simulazione dell'istruzione stosw per ram video
; (gestisce l'accesso alla ram video in contesa con il controller video)
; -----------------------------------------------------------------------
;
vstosw   proc      near
         test      cs:flags,00001000b  ; color video active ?
         jz        nocolor             ; no
         push      dx                  ; yes work to avoid flicker ...
         push      ax
         mov       dx,03dah
waitw1:  in        al,dx
         rcr       al,1
         jb        waitw1
         cli
waitw2:  in        al,dx
         rcr       al,1
         jnb       waitw2
         pop       ax
         stosw
         sti
         pop       dx
         ret
nocolor: stosw
         ret
vstosw   endp
;
; -----------------------------------------------------------------------
;   converte il numero in al in decimale ascii lo pone nel buffer decbuf
; -----------------------------------------------------------------------
;
bindec   proc      near
         cld
         push      ax
         push      cx
         push      es
         push      di
         push      ax
         mov       bx,cs
         mov       es,bx
         mov       di,offset decbuf
         mov       ax,3030h
         stosw
         stosw
         pop       ax
         std
         mov       di,offset decbuf+2
         mov       cl,10
rconv:   mov       ah,0
         div       cl
         xchg      al,ah
         add       al,'0'
         stosb
         sub       al,'0'
         xchg      al,ah
         cmp       al,10
         jge       rconv
         add       al,'0'
         stosb
         pop       di
         pop       es
         pop       cx
         pop       ax
         cld
         ret
bindec   endp
;
; -----------------------------------------------------------------------
;   start bell sound
; -----------------------------------------------------------------------
;
Bell_On  proc      near
         mov       al,0b6h             ; select channel 2,mode 3,binary
         out       43h,al
         mov       bx,freq             ; frequency counter value
         mov       al,bl
         out       42h,al              ; write LSB
         mov       al,bh
         out       42h,al              ; write MSB
         in        al,61h              ; read current state of PB port
         or        al,3                ; enable speaker
         out       61h,al              ; send command to 8255
         or        cs:TimeCol,80h
         inc       cs:[wBeepCnt]
         ret
Bell_On  endp
;
; -----------------------------------------------------------------------
;   stop bell sound
; -----------------------------------------------------------------------
;
Bell_Off proc      near
         in        al,61h
         and       al,0fch
         out       61h,al
         and       cs:TimeCol,7Fh
         cmp       cs:[wBeepCnt], 90*4/2    ; 20 seconds before end ringing
          jb       noresbel
         and       cs:flags, 11111011b      ; shut up bell
         mov       cs:[wBeepCnt], 0         ; reset ringing time counter
         test      cs:flags, 10000000b      ; IPL should be performed?
          jz       noresbel
;
; -----------------------------------------------------------------------
;   perform IPL
; -----------------------------------------------------------------------
;
         mov   bx, 0040h
         mov   ds, bx
         mov   bx, 0072h
         mov   word ptr [bx], 1234h        ; warm reboot
         db    0eah
         dd    0ffff0000h                  ; jmp to FFFF:0000
noresbel:
         ret
Bell_Off endp
;
; -----------------------------------------------------------------------
;   get Dos shift/toggle keys status
; -----------------------------------------------------------------------
;
GetKstat Proc      near
         push      ds
         push      si
         mov       ax,40h              ; dos vars area
         push      ax
         pop       ds
         mov       si,17h              ; shift and toggle keys flags
         lodsb
         pop       si
         pop       ds
         ret
GetKStat endp
;
enpgm:                                 ; end of resident portion
;
; -----------------------------------------------------------------------
;  messages
; -----------------------------------------------------------------------
;
psp_ds   dw        ?                   ; data segment prog. segment prefix
mess00   db        'VCLOCK 3.01  (C) 1987-1996 by Fiorese Franco',7,10,10,13,'$'
mess01   db        10,13,'VCLOCK already installed, time/alarm reset performed',10,13,'$'
mess02   db        10,13,'VCLOCK installed, current time is the system time'
         db        10,13,'$'
mess03   db        'invalid alarm time specified!',7,10,13,'$'
mess04   db        ' time set, alarm is on',10,13,'$'
mess05   db        ' invalid switch!',7,10,13,'$'
mess06   db        ' alarm disabled',7,10,13,'$'
mess07   db        'usage example:',10,13,7
         db        ' "VCLOCK !07:24" set timer auto-reboot alarm to 07:24',10,13
         db        ' "VCLOCK 07:24" set timer alarm to 07:24',10,13
         db        ' "VCLOCK -"     disable timer alarm',10,13
         db        ' (time is if 24h format)', 10,13
         db        'to stop alarm sound press both shift keys',10,13
         db        'to set display time on press both right Shift + Ctrl', 10, 13
         db        'to set display time off press both right Shift + Alt', 10, 13
         db        'to reset VCLOCK time execute dos TIME command and '
         db        'restart VCLOCK as shown above',10,13,'$'
init:
               mov     dx,offset mess00    ; greeting message
               mov     ah,9
               int     21h
               mov     ax,ds
               mov     cs:psp_ds,ax
               push    ds
;
; -- check if already installed
;
               xor     ax, ax
               mov     ds, ax
               mov     ax, offset start
               mov     bx, 70h
               mov     si, [bx]
               mov     cx, [bx+2]
               mov     ds, cx
               add     si, (magic - start)
               lodsw
               cmp     ax, 1234h
               pop     ds
                jne    notinst
               push    ds
               mov     ds, cx
               call    Set_Time
               call    Set_Bell
               pop     ds
               mov     dx,offset mess01
               mov     ah,9
               int     21h
               int     20h
;
;   get current active video
;
notinst: mov       cs:flags, 00010000b  ; set display flag
         mov       dx,0b800h
         or        cs:flags,00001000b    ; set color video flag
         push      dx
         mov       ah,0fh
         int       10h
         pop       dx
         cmp       al,7
         jnz       mcolor
         mov       dx,0b000h
         and       cs:flags,11110111b    ; reset color video flag (no color)
mcolor:  mov       cs:VidSeg,dx          ; dx = segment address area video
;
;   set time
;
         push      cs
         pop       ds
         call      Set_Time
         call      Set_Bell
         mov       ax,offset start
         mov       cs:word ptr [start],90h
         mov       cs:word ptr [start+1],90h
         mov       cs:byte ptr [start+2],0fbh
         cli
         xor       ax,ax
         mov       ds,ax
         mov       ax,offset start
         mov       bx,70h
         mov       [bx],ax
         mov       ax,cs
         mov       [bx+2],ax
         sti
         push      cs
         pop       ds
         mov       dx,offset mess02
         mov       ah,9
         int       21h
         mov       dx,offset enpgm
         inc       dx
         inc       dx
         int       27h
vclock   endp
;
; -----------------------------------------------------------------------
;   set time by DOS
; -----------------------------------------------------------------------
;
Set_Time proc      near
         mov       ah,2ch
         int       21h
         mov       CountSS,dh
         mov       CountMM,cl
         mov       CountHH,ch
         mov       al,dh
         xor       ah,ah
         mov       cl,182
         mul       cl
         mov       cx,10
         xor       dx,dx
         div       cx
         cmp       dx,5
         jb        noround             ; round
         inc       ax
noround:
         mov       CountSC,ax
         mov       cl,91
         div       cl
         mov       CountTK,ah
         ret
Set_Time endp
;
; -----------------------------------------------------------------------
;   set alarm time if specified
; -----------------------------------------------------------------------
;
Set_Bell proc      near
         push      es
         push      ds
         pop       es
         mov       ax,cs:psp_ds
         mov       ds,ax
         mov       si,80h
         lodsb
         cmp       al,2
         je        switch
         cmp       al,6
         je        parsdig1
         cmp       al,7
         jne       helpmsg
         lodsb
         cmp       al,' '
         jne       erparm
         lodsb
         cmp       al, '!'
         or        es:flags,10000000b  ; enable IPL on timer expiration
         jmp       parsdig2

parsdig1:
         lodsb
         cmp       al,' '
         jne       erparm

parsdig2:
         lodsw
         call      Dec2Bin
         jc        erparm
         cmp       al,23
         ja        erparm
         mov       es:AlarmHH,al
         lodsb
         cmp       al,':'
         jne       erparm
         lodsw
         call      Dec2Bin
         jc        erparm
         cmp       al,59
         ja        erparm
         mov       es:AlarmMM,al
         mov       dx,offset mess04
         jmp       short dispmsg
erparm:
         mov       dx,offset mess03
         jmp       short endsubr
dispmsg:
         or        es:flags,00000010b  ; enable alarm
         jmp       short endsubr

helpmsg: mov       dx,offset mess07
         jmp       short endsubr
erswtch:
         mov       dx,offset mess05    ; error invalid switch
         jmp       short endsubr
switch:
         inc       si
         lodsb
         cmp       al,'-'
         jne       erswtch
         and       es:flags,11111101b  ; disable alarm
         mov       dx,offset mess06
endsubr:
         push      cs
         pop       ds
         mov       ah,9
         int       21h
         pop       es
         ret
Set_Bell endp
;
; -----------------------------------------------------------------------
;   convert 2 ASCII digits (AX) into one byte binary number (AL)
; -----------------------------------------------------------------------
;
Dec2Bin  proc      near
         sub       al,30h
         sub       ah,30h
         cmp       al,9
         ja        digerr
         cmp       ah,9
         ja        digerr
         push      ax
         mov       cl,10
         mul       cl
         pop       dx
         add       al,dh
         clc                           ; ok, no errors
         ret
digerr:  stc                           ; error, no valid digits
         ret
Dec2Bin  endp
;
code     ends
         end       start

; =======================================================================
;   GLOBAL -- macros and global definitions
; =======================================================================
;
; -----------------------------------------------------------------------
;   defines used by macros
; -----------------------------------------------------------------------
;
USE386 = 1
;
IFDEF USE286
  .286
ELSEIF USE386
  .386
ENDIF
;
$$MDABIOSINT   equ     61h             ; default MDABIOS interrupt number
;
; =======================================================================
;   .push -- pushes registers, variables, or flags onto the stack.
; -----------------------------------------------------------------------
;   SYN:   .push   "loc16"[,"loc16"...]
;
;      Where:  "loc16" is a 16-bit register, a word-sized variable,
;              or the keyword `FLAGS`.
;
;   EXMPL:     .push   ax,bx,cx,flags,var1 ;ax pushed first...
;              ...
;              .pop    ax,bx,cx,flags,var1 ;...ax popped last
; =======================================================================
;
.push          macro   r0,r1,r2,r3,r4,r5,r6,r7,r8,r9,r10
  IFNB <r10>                                ;; error if too many arguments
.error  <.PUSH has more than 10 arguments>
  ENDIF
  irp $reg,<r0,r1,r2,r3,r4,r5,r6,r7,r8,r9>
    IFB <$reg>                              ;; if at end of list
      exitm                                 ;;   exit the macro
    ELSEIFIDNI <$reg>,<FLAGS>               ;; if it is 'FLAGS'
        pushf                               ;;   push the flags
    ELSE                                    ;; else, must be reg or var
        push    $reg                        ;;   so push it
    ENDIF
  endm
endm
;
;
; =======================================================================
;   .pop -- pops registers, variables, or flags from the stack.
; -----------------------------------------------------------------------
;   SYN:   .pop    "loc16"[,"loc16"...]
;
;      Where:  "loc16" is a 16-bit register, a word-sized variable,
;          or the keyword `FLAGS`.
;
;   EXMPL:     .push   ax,bx,cx,flags,var1 ;ax pushed first...
;              ...
;              .pop    ax,bx,cx,flags,var1 ;...ax popped last
; =======================================================================
;
.pop           macro   r0,r1,r2,r3,r4,r5,r6,r7,r8,r9,r10
  IFNB <r10>                                ;;error if too many arguments
.error  <.POP has more than 10 arguments>
  ENDIF
  irp $reg,<r9,r8,r7,r6,r5,r4,r3,r2,r1,r0>
    IFNB <$reg>                             ;; for each nonblank argument:
      IFIDNI <$reg>,<FLAGS>                 ;; if it is 'FLAGS'
        popf                                ;;   pop the flags
      ELSE                                  ;; else, must be reg or var
        pop $reg                            ;;   so pop it
      ENDIF
    ENDIF
  endm
endm
;
;
; =======================================================================
;   .iowait -- insert delay after an I/O on 80x86 processor
; =======================================================================
;
.iowait        macro
               jmp     $+2             ;; this is a sufficient delay
               jmp     $+2
               endm
;
;
; =======================================================================
;   .iozwait -- delay with cx equal to 0 check
; =======================================================================
;
.iozwait       macro
               jcxz    $+2
               endm
;
;
; =======================================================================
;   .terminate -- terminate the process and return to dos with an error code
; =======================================================================
;
.terminate     macro   @retcod
               ifnb   <@retcod>
                 mov     al,@retcod
               endif
               mov     ah,4ch          ;; terminate process function
               int     21h
               endm
;
;
; =======================================================================
;   .exchange -- exchange segment registers contents
; =======================================================================
;
.xseg          macro   @rseg1, @rseg2
               push    @rseg1
               push    @rseg2
               pop     @rseg1
               pop     @rseg2
               endm
;
;
; =======================================================================
;   .xchg_ptrs -- exchange far pointers
; =======================================================================
;
.xchg_ptrs     macro
               .push   ds,es
               .pop    es,ds
               xchg    di,si
endm
;
; =======================================================================
;   .moveseg -- move between segment registers
; =======================================================================
;
.movseg        macro   @rseg1, @rseg2
               push    @rseg2
               pop     @rseg1
               endm
;
; =======================================================================
;   .jmpe -- adjust short conditional jump (eq)
; =======================================================================
;
.jmpe          macro   @label
               local   @skip
               jne     @skip
               jmp     @label
@skip:
               endm
;
; =======================================================================
;   .jmpne -- adjust short conditional jump (ne)
; =======================================================================
;
.jmpne         macro   @label
               local   @skip
               je      @skip
               jmp     @label
@skip:
               endm
;
;
; =======================================================================
;   .set_intvect -- set DOS interrupt vector
; =======================================================================
;
.set_intvect   macro   @intnum, @handler_ofs, @handler_seg
               mov     dx,offset @handler_ofs
               mov     ax,@handler_seg
               mov     ds,ax
               mov     al,@intnum
               mov     ah,25h
               int     21h
               endm
;
;
; =======================================================================
;   .get_intvect -- get a DOS interrupt vector
; =======================================================================
;
.get_intvect   macro   @intnum
               mov     ax,3500h + (@intnum and 0FFh)
               int     21h
               endm
;
; =======================================================================
;   .disp_hex -- display AL content with AH color at specified position
; =======================================================================
;
.disp_hex      macro   @value,@color,@posy,@posx
               push    ax
               push    di
               mov     al,byte ptr @value
               mov     ah,@color
               mov     di,((((@posy)-1)*80)+((@posx)-1))*2
               call    _hex_disp
               pop     di
               pop     ax
               endm
;
; =======================================================================
;   .disp -- display AL content with AH color at specified position
; =======================================================================
;
.disp          macro   @value,@color,@posy,@posx
               push    ax
               push    di
               mov     al,byte ptr @value
               mov     ah,@color
               mov     di,((((@posy)-1)*80)+((@posx)-1))*2
               call    _disp
               pop     di
               pop     ax
               endm
;
; =======================================================================
;   .mda_reset -- reset MDABIOS
; =======================================================================
;
.mda_reset     macro
               push    ax
               mov     ah,00h       ;; use mov instead xor to not destroy flags
               int     $$MDABIOSINT
               pop     ax
               endm
;
; =======================================================================
;   .mda_print -- write to MDABIOS string/values/registers
; =======================================================================
;
.mda_print     macro   @0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10
               ifnb <@10>
                 %out  .MDA_PRINT has more than 10 arguments
                 .err
               endif
                 irp $val,<@0,@1,@2,@3,@4,@5,@6,@7,@8,@9>
                   ifb <$val>                           ;; if at end of list
                     exitm                              ;;   exit the macro
                   endif
                   $$fc SUBSTR <$val>,1,1
                   $$sl SIZESTR <$val>
                   $$cb INSTR 2,<$val>,<">
                   ifidn $$fc,<">
                     ife $$cb
                       %out .MDA_PRINT has an unterminated string
                       .err
                       exitm
                     endif
                   endif
                   push    dx                           ;; save dx
                   ifidni <$val>,<FLAGS>                ;; if it is 'FLAGS'
                     push  ax
                     pushf
                     pop   dx
                     mov   ax,1304h
                     int   $$MDABIOSINT
                     pop   ax
                   elseifidni <$val>,<LF>               ;; if it is 'LF'
                     push  ax
                     mov   ax,0e0ah
                     int   $$MDABIOSINT
                     pop   ax
                   elseifidni <$val>,<CR>               ;; if it is 'CR'
                     push  ax
                     mov   ax,0e0dh
                     int   $$MDABIOSINT
                     pop   ax
                   elseifidni <$val>,<BELL>             ;; if it is 'BELL>
                     push  ax
                     mov   ax,0e07h
                     int   $$MDABIOSINT
                     pop   ax
                   elseifidn $$fc,<">
                     push  ax
                     jmp   short ($+$$sl+1)
                     db    $val                         ;; allocate string
                     db    0                            ;; string terminator
                     push  di
                     push  es
                     push  cs
                     pop   es
                     lea   di,($-$$sl+3-6)
                     mov   ax,1302h
                     int   $$MDABIOSINT
                     pop   es
                     pop   di
                     pop   ax
                   else
                     push  ax
                     $$r8 instr <al ah bl bh cl ch dl ch>,<$val>
                     $$r6 instr <ax bx cx dx si di bp ds es ss cs>,<$val>
                     if $$r8 or $$r6
                       $$et = 0
                     else
                       $$et = type $val
                     endif
                     if ($$et eq 1)
                       mov   dl,$val                  ;; 8-bit value
                       mov   ax,1304h
                     elseif ($$et eq 2)
                       mov   dx,$val                  ;; 16-bit value
                       mov   ax,1305h
                     elseif ($$et eq 0)
                       if $$r8
                         ifdifi <$val>,<dl>           ;; if dl avoid reload
                           mov   dl,$val              ;; 8-bit register
                         endif
                         mov   ax,1304h
                       elseif $$r6
                         ifdifi <$val>,<dx>           ;; if dx avoid reload
                           mov   dx,$val              ;; 16-bit register
                         endif
                         mov   ax,1305h
                       endif
                     else
                       %out .MDA_PRINT invalid type
                       .err
                       exitm
                     endif
                     int   $$MDABIOSINT
                     pop   ax
                   endif
                   pop     dx                         ;; restore dx
                 endm
               endm
;
; =======================================================================
;   .enter --
; =======================================================================
;
.enter  MACRO   localspace
    IFDEF USE286
        enter   localspace, 0
    ELSE
        push    bp
        mov     bp, sp
        sub     sp, localspace
    ENDIF
ENDM

;
; =======================================================================
;   .leave --
; =======================================================================
;
.leave  MACRO   argspace
    IFDEF USE286
        leave
    ELSE
        mov     sp, bp
        pop     bp
    ENDIF
    IFNB <argspace>
        ret     argspace
    ELSE
        ret
    ENDIF

ENDM


; =======================================================================
;   sprtrap -- traps int 10h for spr.bat use
;
;   by Franco Fiorese 1989
; =======================================================================
;
code           segment
sprtrap        proc    far
               assume  cs:CODE,DS:CODE,ES:CODE
               org     100h
start:
               jmp     init
oldint10       label   dword
int10_ofs      dw      0
int10_seg      dw      0
;
int_10_trap:

               cmp     ax, 0013h        ; check for video mode 80x25 mono
                jnz    skip_this
               mov     ax, 005fh
skip_this:
               pushf
               call    dword ptr cs:[oldint10]
               iret
init:
               mov     ax,cs
               mov     ax,3510h
               int     21h
               mov     cs:[int10_ofs],bx
               mov     cs:[int10_seg],es
               push    ds
               mov     ax,cs
               mov     ds,ax
               mov     dx,offset int_10_trap
               mov     ax,2510h
               int     21h
               pop     ds
               mov     dx,offset init
               int     27h
sprtrap        endp
code           ends
               end     start
